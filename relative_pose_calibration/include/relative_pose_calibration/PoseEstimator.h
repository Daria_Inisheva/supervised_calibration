#ifndef POSEESTIMATOR_H
#define POSEESTIMATOR_H
#include "flat_pattern_calibration/UnitPosition.h"
#include "relative_pose_calibration/PositionedSystem.h"
#include <memory>
#include <queue>
#include <sophus/se3.hpp>
#include <vector>

struct Constraint {
  Constraint(const std::shared_ptr<Camera> &c,
             const std::shared_ptr<FlatPattern> &fp,
             const std::shared_ptr<PositionedSystem> &ps)
      : camera(c), pattern(fp), system(ps) {}
  Constraint() {}
  std::shared_ptr<Camera> camera;
  std::shared_ptr<FlatPattern> pattern;
  std::shared_ptr<PositionedSystem> system;
  int isResolved();
  std::vector<std::weak_ptr<Constraint>> solve();

  Sophus::SE3d getPatternTocam();

  friend bool operator<(const Constraint &a, const Constraint &b) {
    return a.pattern->size() < b.pattern->size();
  }
  friend bool operator>(const Constraint &a, const Constraint &b) {
    return a.pattern->size() > b.pattern->size();
  }
};

#endif // POSEESTIMATOR_H

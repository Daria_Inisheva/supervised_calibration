#include <gtest/gtest.h>
#include <iostream>
#include <memory>
#include <random>
#include <relative_pose_calibration/CameraSystemCalibration.h>
class CameraSystemtest : public ::testing::Test {

protected:
  void SetUp() {
    createCams(5);

    generate_pattern(12);
  }

  void createCams(const int &N);

  void fill_withCamera(FlatPattern &fp, const std::shared_ptr<Camera> &cam);

  void generate_pattern(const int &PatternN);

  bool is_visible(const FlatPattern &fp,
                  const std::vector<std::shared_ptr<Camera>> &cam,
                  const Eigen::Matrix3d &R, const Eigen::Vector3d &T);

  bool is_visible(const FlatPattern &fp, const std::shared_ptr<Camera> &cam,
                  const Sophus::SE3d &pose, const Eigen::Matrix3d &R,
                  const Eigen::Vector3d &T);

  bool is_visible(const Eigen::Vector3d &p, const std::shared_ptr<Camera> &cam);

  Sophus::SE3d getMM(std::uniform_real_distribution<double> &angle,
                     std::mt19937 &rng,
                     std::uniform_real_distribution<double> &runif);
  Sophus::SE3d getMM(const Eigen::Matrix3d &R, const Eigen::Vector3d &T);

  void TearDown() {}
  SystemCalibrationProblem p;
  std::vector<std::shared_ptr<Camera>> cams;
  std::vector<Sophus::SE3d> poses;
};

void CameraSystemtest::fill_withCamera(FlatPattern &fp,
                                       const std::shared_ptr<Camera> &cam) {
  int Size = fp.size();
  for (int i = 0; i < Size; ++i) {
    Eigen::Vector3d ray = fp.getPattern(i);
    ray = fp.patternToCam * ray;
    Eigen::Vector2d pix = cam->cam->project(ray);
    fp.Points[i].first = pix;
  }
}

void CameraSystemtest::createCams(const int &N = 3) {
  /// GENERATING CAMERAS
  int w = 4000, h = 3000;
  // Omnidirectional
  Eigen::Vector2d stretch(0.99996, 0);
  Eigen::Vector2d principal(2085.42, 1633.51);
  Eigen::VectorXd coeff(5);
  coeff << 2971.02, -7.65311e-06, 5.29748e-08, -3.24166e-11, 4.59622e-15;
  std::uniform_real_distribution<double> angle(-0.5, 0.5);
  std::mt19937 rng;
  std::uniform_real_distribution<double> runif(0, 1);

  auto idP = getMM(Eigen::Matrix3d::Identity(), Eigen::Vector3d(0, 0, 2));

  auto cam =
      p.createCamera(std::shared_ptr<CameraModel>(
                         new OmnidirectionalModel(stretch, principal, coeff)),
                     idP);
  cam->cam->width = w;
  cam->cam->height = h;
  cams.push_back(cam);
  poses.push_back(idP);
  for (int i = 1; i < N; ++i) {
    auto pose = getMM(angle, rng, runif);
    poses.push_back(pose);
    cams.push_back(p.createCamera(std::shared_ptr<CameraModel>(
        new OmnidirectionalModel(stretch, principal, coeff))));
    cams[i]->cam->width = w;
    cams[i]->cam->height = h;
  }
}

void CameraSystemtest::generate_pattern(const int &PatternN) {

  std::uniform_real_distribution<double> angle(0, 0.5);
  std::mt19937 rng;
  std::uniform_real_distribution<double> runif(-2, 2);

  std::shared_ptr<Pattern> pat = p.createPattern();
  const int PN = 3;
  const int step = 1;
  FlatPattern fp;
  for (int x = 0; x < PN; ++x)
    for (int y = 0; y < PN; ++y) {
      V3 pt = V3((x - PN / 2) * step, (y - PN / 2) * step, 1);
      fp.addPair({Eigen::Vector2d(0, 0), pt});
    }

  Eigen::Matrix3d R;
  Eigen::Vector3d T;
  for (int i = 0; i < PatternN; ++i) {
    // Pattern R, T generator
    do {
      T = Eigen::Vector3d(runif(rng), runif(rng), runif(rng));
      R = Sophus::SO3d::exp(V3(angle(rng), angle(rng), angle(rng))).matrix();
    } while (!is_visible(fp, cams, R, T));

    systemConnection camConn;
    Sophus::SE3d PatternToSystem = getMM(R, T);
    std::cout << "___________________" << std::endl;
    for (size_t j = 0; j < cams.size(); ++j) {

      if (!is_visible(fp, cams[j], poses[j], R, T))
        continue;
      std::cout << "Visible from " << j << " cam" << std::endl;
      auto fp1 = fp;
      fp1.patternToCam = poses[j] * PatternToSystem;

      fill_withCamera(fp1, cams[j]);
      auto FPpoint = p.createFlatPattern(pat, fp1.Points, fp1.patternToCam);
      camConn.push_back({cams[j], FPpoint});
    }

    auto sys1 = p.createSystem(camConn);
    for (auto k : camConn)
      p.addObservation(k.first, sys1, k.second);
  }
  std::cout << "Fp generated\n";
}

bool CameraSystemtest::is_visible(
    const FlatPattern &fp, const std::vector<std::shared_ptr<Camera>> &cam,
    const Eigen::Matrix3d &R, const Eigen::Vector3d &T) {
  int n = 0;
  for (size_t i = 0; i < cams.size(); ++i) {
    if (is_visible(fp, cams[i], poses[i], R, T))
      n++;
  }
  if (n >= 2)
    return true;

  return false;
}

bool CameraSystemtest::is_visible(const FlatPattern &fp,
                                  const std::shared_ptr<Camera> &cam,
                                  const Sophus::SE3d &pose,
                                  const Eigen::Matrix3d &R,
                                  const Eigen::Vector3d &T) {
  for (int i = 0; i < fp.size(); ++i) {
    Eigen::Vector3d ray = fp.getPattern(i);

    auto backPose = getMM(R, T);
    ray = backPose * ray;
    ray = pose * ray;

    if (!is_visible(ray, cam)) {
      return false;
    }
  }

  return true;
}

bool CameraSystemtest::is_visible(const Eigen::Vector3d &p,
                                  const std::shared_ptr<Camera> &cam) {
  Eigen::Vector2d pixel = cam->cam->project(p);
  if (!std::isfinite(pixel(0)) || !std::isfinite(pixel(1)))
    return false;
  if (pixel(0) < 0 || pixel(0) >= cam->cam->width)
    return false;
  if (pixel(1) < 0 || pixel(1) >= cam->cam->height)
    return false;
  return true;
}

Sophus::SE3d
CameraSystemtest::getMM(std::uniform_real_distribution<double> &angle,
                        std::mt19937 &rng,
                        std::uniform_real_distribution<double> &runif) {

  Eigen::Matrix3d R =
      Sophus::SO3d::exp(V3(angle(rng), angle(rng), angle(rng))).matrix();
  Eigen::Vector3d T(runif(rng), runif(rng), runif(rng));

  auto se = getMM(R, T);
  return se;
}
Sophus::SE3d CameraSystemtest::getMM(const Eigen::Matrix3d &R,
                                     const Eigen::Vector3d &T) {
  Eigen::Matrix4d se;
  se.setZero();
  se(3, 3) = 1;
  se.block(0, 0, 3, 3) = R;
  se.col(3).head(3) = T;
  return Sophus::SE3d::fitToSE3(se);
}

void printPose(const Sophus::SE3d &a, const std::string &name) {
  std::cout << name + '\n'
            << a.rotationMatrix() << std::endl
            << a.translation().transpose() << std::endl;
}

TEST_F(CameraSystemtest, DUMMYTEST) {
  p.calibrate();
  std::cout << p.constr.size() << " size of const" << std::endl;
  int count = 0;
  for (auto pose : poses)
    printPose(pose, "pose " + std::to_string(count++));
  count = 0;
  for (auto cam : cams)
    printPose(cam->pose, "pose " + std::to_string(count++));
  for (size_t i = 0; i < cams.size(); ++i) {
    auto tempPose = poses[i].inverse() * cams[i]->pose;
    double rotLog = tempPose.so3().log().norm();
    double tranLog = tempPose.translation().norm();
    EXPECT_GE(1e-5, rotLog);
    EXPECT_GE(1e-5, tranLog);

    std::cout << i << " " << (poses[i].inverse() * cams[i]->pose).log().norm()
              << std ::endl;
    ;
  }
}

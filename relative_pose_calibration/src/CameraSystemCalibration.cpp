#include "relative_pose_calibration/CameraSystemCalibration.h"
#include "relative_pose_calibration/RelativePoseFunctor.h"
#include <map>
#include <set>
#include <thread>

std::shared_ptr<Pattern> SystemCalibrationProblem::createPattern() {
  return std::shared_ptr<Pattern>(new Pattern());
}

std::shared_ptr<Pattern>
SystemCalibrationProblem::createPattern(const Sophus::SE3d &pose) {
  return std::shared_ptr<Pattern>(new Pattern(pose));
}

std::shared_ptr<PositionedSystem>
SystemCalibrationProblem::createSystem(systemConnection &connection) {
  return std::shared_ptr<PositionedSystem>(new PositionedSystem(connection));
}

std::shared_ptr<Camera> SystemCalibrationProblem::createCamera(
    const std::shared_ptr<CameraModel> &cam) {
  return std::shared_ptr<Camera>(new Camera(cam));
}

std::shared_ptr<FlatPattern>
SystemCalibrationProblem::createFlatPattern(std::shared_ptr<Pattern> &pat,
                                            const FlatPattern::PatternVector &p,
                                            const Sophus::SE3d &pTC) {
  return std::shared_ptr<FlatPattern>(new FlatPattern(pat, p, pTC));
}

std::shared_ptr<FlatPattern> SystemCalibrationProblem::createFlatPattern(
    std::shared_ptr<Pattern> &pat, const FlatPattern::PatternVector &p) {
  return std::shared_ptr<FlatPattern>(new FlatPattern(pat, p));
}

std::shared_ptr<FlatPattern>
SystemCalibrationProblem::createFlatPattern(std::shared_ptr<Pattern> &pat) {
  return std::shared_ptr<FlatPattern>(new FlatPattern(pat, {}));
}

std::shared_ptr<Camera>
SystemCalibrationProblem::createCamera(const std::shared_ptr<CameraModel> &cam,
                                       const Sophus::SE3d &pose) {
  return std::shared_ptr<Camera>(new Camera(cam, pose));
}

void SystemCalibrationProblem::addObservation(
    const std::shared_ptr<Camera> &c,
    const std::shared_ptr<PositionedSystem> &ps,
    const std::shared_ptr<FlatPattern> &fp) {
  constr.push_back(std::shared_ptr<Constraint>(new Constraint(c, fp, ps)));
}

void SystemCalibrationProblem::addObservation(systemConnection &c) {
  auto sys = createSystem(c);
  for (auto k : c)
    addObservation(k.first, sys, k.second);
}

void SystemCalibrationProblem::initializec() {
  bool fixCam = false, fixPatt = false;
  for (auto c : constr) {
    if (c->camera->isFix())
      fixCam = true;
    if (c->pattern->pattern->isFix())
      fixPatt = true;
  }

  if (!fixCam) {
    (*camSet.begin())->isFixed = true;
    (*camSet.begin())->known = true;
  }
  if (!fixPatt) {
    (*patSet.begin())->isFixed = true;
    (*patSet.begin())->known = true;
  }

  static int Ind = 0;
  for (auto c : constr) {
    c->camera->addNeighbour(c);
    c->pattern->pattern->addNeighbour(c);
    c->system->addNeighbour(c);
    if (c->isResolved() >= 2)
      queue.push(c);
  }

  if (queue.empty()) {
    bool knownOne = false;
    for (auto c : constr) {
      if (c->camera->known) {
        c->pattern->pattern->known = true;
        knownOne = true;
        break;
      }
      if (c->pattern->pattern->known) {
        c->camera->known = true;
        knownOne = true;
        break;
      }
    }
    if (!knownOne) {
      constr[Ind]->camera->known = true;
      constr[Ind]->pattern->pattern->known = true;
    }
    for (auto c : constr)
      if (c->isResolved() >= 2)
        queue.push(c);

    Ind++;
  }
}

bool SystemCalibrationProblem::fullResolved() {
  for (auto c : constr)
    if (c->isResolved() != 3)
      return false;
  return true;
}

void SystemCalibrationProblem::initializePoses() {
  if (constr.size() == 0)
    return;
  initializec();
  if (queue.size() == 0) {
    LOG(ERROR) << "COULDN`T RESOLVE EVEN 1 CONSTRAINT";
    return;
  }
  int sysID = 0;
  while (!(fullResolved())) {

    if (queue.empty()) {
      for (const auto &c : constr) {
        if (c->isResolved() == 1 && !c->pattern->pattern->known) {
          c->pattern->pattern->known = true;
          queue.push(c);
          break;
        }
      }
    }
    if (queue.empty()) {
      LOG(ERROR) << "Something is wrong with data";
      return;
    }
    while (!queue.empty()) {
      if (queue.top()->isResolved() == 3)
        queue.pop();

      auto connect = queue.top()->solve();
      queue.top()->pattern->pattern->sysID = sysID;
      for (auto i : connect) {
        auto c = i.lock();
        if (c->isResolved() == 2)
          queue.push(c);
      }
    }
    sysID++;
  }
}

void SystemCalibrationProblem::calibrate() {
  // To fill 3 sets with a purpose to adding
  // them to ParameterBlock
  for (const auto &c : constr) {
    camSet.insert(c->camera);
    patSet.insert(c->pattern->pattern);
    sysSet.insert(c->system);
  }

  initializePoses();

  optimizeRotation();
  optimizeTranslation();
  fullOptimization();

  if (loss_ != nullptr)
    fullOptimization(loss_);
}

void SystemCalibrationProblem::optimizeRotation() {
  ceres::Problem problem;
  std::cout << "\n Starting rotation optimization\n";

  const int N = 4;
  typedef Sophus::LocalParameterizationSO3 Local;
  auto lf = [](const std::shared_ptr<UnitPosition> &pos) {
    return pos->getSO3();
  };
  setParametrization<Local>(problem, N, lf);

  int countN = 0;
  for (const auto &c : constr) {
    if (c->isResolved() != 3)
      continue;
    countN++;
    auto cost = RelativeRotationFunctor::create(c->pattern->patternToCam.so3());
    problem.AddResidualBlock(cost, nullptr, c->pattern->pattern->getSO3(),
                             c->system->getSO3(), c->camera->getSO3());
  }

  ceres::Solver::Options options;
  options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = false;
  options.max_num_iterations = 10000;
  options.parameter_tolerance = 1e-10;
  options.gradient_tolerance = 1e-10;
  options.function_tolerance = 1e-10;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  std::cout << summary.FullReport() << "\n";
  double final_error = sqrt(2 * summary.final_cost / countN);
  std::cout << " Roatation RMSE : " << 180 * final_error / M_PI << std::endl;
}

void SystemCalibrationProblem::optimizeTranslation() {
  ceres::Problem problem;
  std::cout << "\n Starting translation optimization\n";
  const int N = 7;
  typedef Sophus::LocalParameterizationSE3Translation Local;
  auto lf = [](const std::shared_ptr<UnitPosition> &pos) {
    return pos->getSE3();
  };
  setParametrization<Local>(problem, N, lf);

  int countN = 0;
  for (const auto &c : constr) {
    if (c->isResolved() != 3)
      continue;
    countN++;
    auto cost = RelativeTranslationFunctor::create(c->pattern->patternToCam);
    problem.AddResidualBlock(cost, nullptr, c->pattern->pattern->getSE3(),
                             c->system->getSE3(), c->camera->getSE3());
  }

  ceres::Solver::Options options;
  options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = false;
  options.max_num_iterations = 10000;
  options.parameter_tolerance = 1e-10;
  options.gradient_tolerance = 1e-10;
  options.function_tolerance = 1e-10;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  std::cout << summary.FullReport() << "\n";
  double final_error = 2 * summary.final_cost / countN;
  std::cout << " Translation RMSE : " << sqrt(final_error) << std::endl;
}

void SystemCalibrationProblem::fullOptimization(ceres::LossFunction *loss) {
  ceres::Problem problem;
  std::cout << "\n Starting All in one optimization\n";
  const int N = 7;
  typedef Sophus::LocalParameterizationSE3 Local;
  auto lf = [](const std::shared_ptr<UnitPosition> &pos) {
    return pos->getSE3();
  };
  setParametrization<Local>(problem, N, lf);

  // this is need for counting cost_function for each camera
  // separeate
  std::map<std::string, std::vector<ceres::ResidualBlockId>> resIds;

  for (auto &c : camSet) {
    resIds.insert({c->id, {}});
    double *camD = c->cam->modelParams.data();
    problem.AddParameterBlock(camD, c->cam->modelParams.rows(),
                              c->cam->createLocalParametrization());
  }

  int countN = 0;
  for (const auto &c : constr) {
    if (c->isResolved() != 3)
      continue;
    double *camD = c->camera->cam->modelParams.data();
    for (int i = 0; i < c->pattern->size(); ++i) {
      Eigen::Vector2d pixel = c->pattern->getPixel(i);
      Eigen::Vector3d patternPt = c->pattern->getPattern(i);

      auto cost = c->camera->cam->getGlobalRelativeCost(pixel, patternPt);
      countN++;
      auto rId = problem.AddResidualBlock(
          cost, loss, camD, c->pattern->pattern->getSE3(), c->system->getSE3(),
          c->camera->getSE3());
      resIds[c->camera->id].push_back(rId);
    }
  }
  ceres::Solver::Options options;
  options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = true;
  options.num_threads = std::thread::hardware_concurrency();
  options.max_num_iterations = 1000;
  options.parameter_tolerance = 1e-10;
  options.gradient_tolerance = 1e-10;
  options.function_tolerance = 1e-10;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  std::cout << summary.FullReport() << "\n";
  double final_error = sqrt(2 * summary.final_cost / countN);
  std::cout << "Rel Pose error = " << final_error << std::endl;

  // here we are counting final cost for each camera after
  // non linear relative pose estimation
  std::cout << " New RMSE`s for cameras, after NNL optimization \n ";
  for (auto c : resIds) {
    ceres::Problem::EvaluateOptions options;
    options.residual_blocks = c.second;
    double total_cost = 0.0;
    problem.Evaluate(options, &total_cost, nullptr, nullptr, nullptr);
    std::cout << "id: " << c.first << "\n \t"
              << sqrt(2 * total_cost / c.second.size()) << std::endl;
  }
}

#include <fstream>
void SystemCalibrationProblem::savePatterns() {
  static int num = 2;
  // saving patterns and new patterns :
  for (auto c : constr) {
    std::string fname = c->camera->id + c->pattern->t + "1.csv";
    std::string fname1 =
        c->camera->id + c->pattern->t + std::to_string(num) + ".csv";

    std::ofstream file(fname);
    std::ofstream file1(fname1);
    Sophus::SE3d pose = c->getPatternTocam();
    for (int i = 0; i < c->pattern->size(); ++i) {
      Eigen::Vector2d pix = c->pattern->getPixel(i);
      Eigen::Vector3d ray = c->pattern->getPattern(i);
      ray = pose * ray;
      Eigen::Vector2d newPix = c->camera->cam->project(ray);
      file << pix(0) << "," << pix(1) << std::endl;
      file1 << newPix(0) << "," << newPix(1) << std::endl;
    }
    file.close();
    file1.close();
  }
  num++;
}

#ifndef CAMERACALIBRATION_H
#define CAMERACALIBRATION_H
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <flat_pattern_calibration/AtanFisheyeCalibration.h>
#include <flat_pattern_calibration/FlatPattern.h>
#include <flat_pattern_calibration/OmnidirectionalCalibration.h>
#include <flat_pattern_calibration/PinHoleCalibration.h>

#include <flat_pattern_calibration/CameraCalibration.h>
namespace py = pybind11;

static std::string EigentoString(const Eigen::Matrix<double, -1, -1> &mat) {
  std::stringstream ss;
  ss << mat;
  return ss.str();
}

std::unique_ptr<CameraModel>
createCamera(std::string camName, int w, int h, int radialN = 4,
             int tangentialN = 2, int coeffN = 5, bool fixedFocal = false,
             bool equalFocal = false, bool fixedPrincipal = false,
             bool fixedSkew = false, bool zeroSkew = false,
             bool fixedStretch = false, bool identStretch = false,
             bool diagonalStretch = false, bool fixedPolynom = false,
             bool fixTangential = false, bool fixRadial = false,
             bool fixEvenRadial = false, bool fixOddRadial = false,
             bool fixEvenPolynom = false, bool fixOddPolynom = false) {

  if (camName == "Pinhole") {

    PinHoleCalibrationParams params(radialN, tangentialN);
    params.fixedFocal = fixedFocal;
    params.equalFocal = equalFocal;
    params.fixedPrincipal = fixedPrincipal;
    params.fixedSkew = fixedSkew;
    params.zeroSkew = zeroSkew;
    params.fixedRadial = fixRadial;
    params.fixedTangential = fixTangential;
    params.fixOddRadial = fixOddRadial;
    params.fixEvenRadial = fixEvenRadial;

    return std::unique_ptr<CameraModel>(new PinHoleModel(w, h, params));
  }

  if (camName == "Atan-fisheye") {
    AtanCalibrationParams params(coeffN);
    params.fixedFocal = fixedFocal;
    params.equalFocal = equalFocal;
    params.fixedPrincipal = fixedPrincipal;
    params.fixedSkew = fixedSkew;
    params.zeroSkew = zeroSkew;
    params.fixedPolynom = fixedPolynom;
    params.fixOdd = fixOddPolynom;
    params.fixEven = fixEvenPolynom;
    return std::unique_ptr<CameraModel>(new AtanFisheyeModel(w, h, params));
  }

  if (camName == "Omnidirectional") {
    OmnidirectionalCalibrationParams params(coeffN);
    params.fixedStretch = fixedStretch;
    params.identStretch = identStretch;
    params.diagonalStretch = diagonalStretch;
    params.fixedPrincipal = fixedPrincipal;
    params.fixedPolynom = fixedPolynom;
    params.fixOdd = fixOddPolynom;
    params.fixEven = fixEvenPolynom;
    return std::unique_ptr<CameraModel>(new OmnidirectionalModel(w, h, params));
  }
  return nullptr;
}

double calibrateCamera(const std::shared_ptr<CameraModel> &model,
                       FlatPatterns &patterns, bool with_loss = false) {

  if (model->getType() == "Pinhole") {
    auto cam = std::dynamic_pointer_cast<PinHoleModel>(model);
    PinHoleCalibration calibration(cam, patterns);
    if (with_loss)
      calibration.setLoss(new ceres::CauchyLoss(2.0));
    double error = calibration.calibrate();
    std::cout << "Calibration error = " << error << std::endl;
    return error;
  }
  if (model->getType() == "Atan-fisheye") {
    auto cam = std::dynamic_pointer_cast<AtanFisheyeModel>(model);
    AtanFisheyeCalibration calibration(cam, patterns);
    if (with_loss)
      calibration.setLoss(new ceres::CauchyLoss(2.0));

    double error = calibration.calibrate();
    std::cout << "Calibration error = " << error << std::endl;
    return error;
  }
  if (model->getType() == "Omnidirectional") {
    auto cam = std::dynamic_pointer_cast<OmnidirectionalModel>(model);
    OmnidirectionalCalibration calibration(cam, patterns);
    if (with_loss)
      calibration.setLoss(new ceres::CauchyLoss(2.0));

    double error = calibration.calibrate();
    std::cout << "Calibration error = " << error << std::endl;
    return error;
  }

  return 0.0;
}

#endif // CAMERACALIBRATION_H

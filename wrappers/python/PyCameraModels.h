#ifndef PYCAMERAMODELS_H
#define PYCAMERAMODELS_H
#include <Eigen/Eigen>
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>

#include <camera_models/AtanFisheyeCamera.h>
#include <camera_models/OmnidirectionalCamera.h>
#include <camera_models/PinHoleCamera.h>

class PyCameraModel : public CameraModel,
                      public std::enable_shared_from_this<CameraModel> {
public:
  using CameraModel::CameraModel;
  Eigen::Vector2d project(const Eigen::Vector3d &point) override {
    PYBIND11_OVERLOAD_PURE(Eigen::Vector2d, CameraModel, project, point);
  }
  Eigen::Vector3d projectBack(const Eigen::Vector2d &point) override {
    PYBIND11_OVERLOAD_PURE(Eigen::Vector3d, CameraModel, projectBack, point);
  }
  double convertFromModel(const std::shared_ptr<CameraModel> &from) override {
    PYBIND11_OVERLOAD_PURE(double, CameraModel, convertFromModel, from);
  }
  void print() override { PYBIND11_OVERLOAD_PURE(void, CameraModel, print, ); }
  Eigen::Vector2d getPrincipal() override {
    PYBIND11_OVERLOAD_PURE(Eigen::Vector2d, CameraModel, getPrinciapl, );
  }
  void saveToFile(const std::string &fname) override {
    PYBIND11_OVERLOAD_PURE(void, CameraModel, saveToFile, fname);
  }
  void loadFromFile(const std::string &fname) override {
    PYBIND11_OVERLOAD_PURE(void, CameraModel, loadFromFile, fname);
  }

};

class PyPinHoleModel : public PinHoleModel,
                       public std::enable_shared_from_this<PyPinHoleModel> {
public:
  using PinHoleModel::PinHoleModel;
  Eigen::Vector2d project(const Eigen::Vector3d &point) override {
    PYBIND11_OVERLOAD(Eigen::Vector2d, PinHoleModel, project, point);
  }
  Eigen::Vector3d projectBack(const Eigen::Vector2d &point) override {
    PYBIND11_OVERLOAD(Eigen::Vector3d, PinHoleModel, projectBack, point);
  }
  double convertFromModel(const std::shared_ptr<CameraModel> &from) override {
    PYBIND11_OVERLOAD(double, PinHoleModel, convertFromModel, from);
  }
  void print() override { PYBIND11_OVERLOAD(void, PinHoleModel, print, ); }
  double getSkew() const { PYBIND11_OVERLOAD(double, PinHoleModel, getSkew, ); }
  void setSkew(const double &newSkew) {
    PYBIND11_OVERLOAD(void, PinHoleModel, setSkew, newSkew);
  }
  Eigen::Vector2d getFocal() const {
    PYBIND11_OVERLOAD(Eigen::Vector2d, PinHoleModel, getFocal, );
  }
  void setFocal(const Eigen::Vector2d &newFocal) {
    PYBIND11_OVERLOAD(void, PinHoleModel, setFocal, newFocal);
  }
  V2 getPrincipal() override {
    PYBIND11_OVERLOAD(V2, PinHoleModel, getPrincipal, );
  }
  void setPrincipal(const Eigen::Vector2d &newPrincipal) {
    PYBIND11_OVERLOAD(void, PinHoleModel, setPrincipal, newPrincipal);
  }
  Vx getRadialPolynom() const {
    PYBIND11_OVERLOAD(Vx, PinHoleModel, getRadialPolynom, );
  }
  Vx getTangentialPolynom() const {
    PYBIND11_OVERLOAD(Vx, PinHoleModel, getTangentialPolynom, );
  }
  void exportToMatLab(const std::string &fname) {
    PYBIND11_OVERLOAD(void, PinHoleModel, exportToMatlab, fname);
  }
  void exportToOpencv(const std::string &fname) {
    PYBIND11_OVERLOAD(void, PinHoleModel, exportToOpencv, fname);
  }
  void saveToFile(const std::string &fname) override {
    PYBIND11_OVERLOAD_PURE(void, PinHoleModel, saveToFile, fname);
  }
  void loadFromFile(const std::string &fname) override {
    PYBIND11_OVERLOAD_PURE(void, PinHoleModel, loadFromFile, fname);
  }

};

class PyAtanFisheyeModel
    : public AtanFisheyeModel,
      public std::enable_shared_from_this<PyAtanFisheyeModel> {
public:
  using AtanFisheyeModel::AtanFisheyeModel;
  Eigen::Vector2d project(const Eigen::Vector3d &point) override {
    PYBIND11_OVERLOAD(Eigen::Vector2d, AtanFisheyeModel, project, point);
  }
  Eigen::Vector3d projectBack(const Eigen::Vector2d &point) override {
    PYBIND11_OVERLOAD(Eigen::Vector3d, AtanFisheyeModel, projectBack, point);
  }
  double convertFromModel(const std::shared_ptr<CameraModel> &from) override {
    PYBIND11_OVERLOAD(double, AtanFisheyeModel, convertFromModel, from);
  }
  void print() override { PYBIND11_OVERLOAD(void, AtanFisheyeModel, print, ); }
  double getSkew() const {
    PYBIND11_OVERLOAD(double, AtanFisheyeModel, getSkew, );
  }
  void setSkew(const double &newSkew) {
    PYBIND11_OVERLOAD(void, AtanFisheyeModel, setSkew, newSkew);
  }
  Eigen::Vector2d getFocal() const {
    PYBIND11_OVERLOAD(Eigen::Vector2d, AtanFisheyeModel, getFocal, );
  }
  void setFocal(const Eigen::Vector2d &newFocal) {
    PYBIND11_OVERLOAD(void, AtanFisheyeModel, setFocal, newFocal);
  }
  V2 getPrincipal() override {
    PYBIND11_OVERLOAD(V2, AtanFisheyeModel, getPrincipal, );
  }
  void setPrincipal(const Eigen::Vector2d &newPrincipal) {
    PYBIND11_OVERLOAD(void, AtanFisheyeModel, setPrincipal, newPrincipal);
  }
  Vx getPolynom() const {
    PYBIND11_OVERLOAD(Vx, AtanFisheyeModel, getPolynom, );
  }
  void exportToMatLab(const std::string &fname) {
    PYBIND11_OVERLOAD(void, AtanFisheyeModel, exportToMatlab, fname);
  }
  void exportToOpencv(const std::string &fname) {
    PYBIND11_OVERLOAD(void, AtanFisheyeModel, exportToOpencv, fname);
  }
  void saveToFile(const std::string &fname) override {
    PYBIND11_OVERLOAD_PURE(void, AtanFisheyeModel, saveToFile, fname);
  }
  void loadFromFile(const std::string &fname) override {
    PYBIND11_OVERLOAD_PURE(void, AtanFisheyeModel, loadFromFile, fname);
  }

};

class PyOmnidirectionalModel
    : public OmnidirectionalModel,
      public std::enable_shared_from_this<PyOmnidirectionalModel> {
public:
  using OmnidirectionalModel::OmnidirectionalModel;
  Eigen::Vector2d project(const Eigen::Vector3d &point) override {
    PYBIND11_OVERLOAD(Eigen::Vector2d, OmnidirectionalModel, project, point);
  }
  Eigen::Vector3d projectBack(const Eigen::Vector2d &point) override {
    PYBIND11_OVERLOAD(Eigen::Vector3d, OmnidirectionalModel, projectBack,
                      point);
  }
  double convertFromModel(const std::shared_ptr<CameraModel> &from) override {
    PYBIND11_OVERLOAD(double, OmnidirectionalModel, convertFromModel, from);
  }
  void print() override {
    PYBIND11_OVERLOAD(void, OmnidirectionalModel, print, );
  }
  Eigen::Matrix2d getStretchMatrix() {
    PYBIND11_OVERLOAD(Eigen::Matrix2d, OmnidirectionalModel,
                      getStretchMatrix, );
  }
  void setStretchMatrix(const Eigen::Matrix2d &newStretch) {
    PYBIND11_OVERLOAD(void, OmnidirectionalModel, setStretchMatrix, newStretch);
  }
  V2 getPrincipal() override {
    PYBIND11_OVERLOAD(V2, OmnidirectionalModel, getPrincipal, );
  }
  void setPrincipal(const Eigen::Vector2d &newPrincipal) {
    PYBIND11_OVERLOAD(void, OmnidirectionalModel, setPrincipal, newPrincipal);
  }
  Vx getPolynom() const {
    PYBIND11_OVERLOAD(Vx, OmnidirectionalModel, getPolynom, );
  }
  void exportToMatLab(const std::string &fname) {
    PYBIND11_OVERLOAD(void, OmnidirectionalModel, exportToMatlab, fname);
  }
  void exportToOpencv(const std::string &fname) {
    PYBIND11_OVERLOAD(void, OmnidirectionalModel, exportToOpencv, fname);
  }
  void saveToFile(const std::string &fname) override {
    PYBIND11_OVERLOAD_PURE(void, OmnidirectionalModel, saveToFile, fname);
  }
  void loadFromFile(const std::string &fname) override {
    PYBIND11_OVERLOAD_PURE(void, OmnidirectionalModel, loadFromFile, fname);
  }

};

#endif // PYCAMERAMODELS_H

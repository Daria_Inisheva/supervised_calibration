#ifndef HOMOGRAPHYCOMPUTATION_H
#define HOMOGRAPHYCOMPUTATION_H
#include "camera_models/AtanFisheyeCamera.h"
#include "camera_models/OmnidirectionalCamera.h"
#include "camera_models/PinHoleCamera.h"
#include "flat_pattern_calibration/FlatPattern.h"
#include "flat_pattern_calibration/SnLocalParametrization.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>
#include <glog/logging.h>
#include <vector>

// this function solve equations like
// alpha * X = (r1|r2|t)U
template <typename T>
Eigen::Matrix<T, 3, 3>
//  left = H * right
computeHomographyDLT(const Eigen::Matrix<T, -1, 3> &_left,
                     const Eigen::Matrix<T, -1, 3> &_right) {

  LOG(INFO) << "HomographyDlt leftN " << _left.rows() << " rightN "
            << _right.rows();
  using V3T = Eigen::Matrix<T, 1, 3>;
  Eigen::Matrix<T, -1, 9> A(3 * _left.rows(), 9);
  A.setZero();
  for (int i = 0; i < _left.rows(); ++i) {
    V3T u = _right.row(i), x = _left.row(i);

    A.template block<1, 3>(i * 3, 3) = x[2] * u;
    A.template block<1, 3>(i * 3, 6) = -x[1] * u;
    A.template block<1, 3>(i * 3 + 1, 0) = -x[2] * u;
    A.template block<1, 3>(i * 3 + 1, 6) = x[0] * u;
    A.template block<1, 3>(i * 3 + 2, 0) = x[1] * u;
    A.template block<1, 3>(i * 3 + 2, 3) = -x[0] * u;
  }
  Eigen::Matrix<T, 3, 3> H;
  Eigen::Matrix<T, 9, 1> h = A.jacobiSvd(Eigen::ComputeFullV).matrixV().col(8);
  H << h.template head<3>(), h.template block<3, 1>(3, 0), h.template tail<3>();
  LOG(INFO) << "Estimated Homography is\n" << H.transpose();
  return H.transpose();
}

// Tiny cost function to compute optimized homography
// optimizing on the pattern plane
struct HomographyTinyCost {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  HomographyTinyCost(const Eigen::Vector3d &left, const Eigen::Vector3d &right)
      : left(left), right(right) {}

  template <typename T> bool operator()(const T *h, T *residuals) const {
    Eigen::Map<const Eigen::Matrix<T, 3, 3>> H(h);
    Eigen::Map<Eigen::Matrix<T, 2, 1>> res(residuals);
    res =
        (H * right.cast<T>()).hnormalized() - left.template head<2>().cast<T>();
    return true;
  }

  Eigen::Vector3d left;
  Eigen::Vector3d right;
  static ceres::CostFunction *Create(const Eigen::Vector3d &left,
                                     const Eigen::Vector3d &right) {
    return (new ceres::AutoDiffCostFunction<HomographyTinyCost, 2, 9>(
        new HomographyTinyCost(left, right)));
  }
};

// Small homography cost, which optimize it on the image plane
template <typename CamModel> struct HomographyModelCost {

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  HomographyModelCost(const Eigen::Vector2d &image,
                      const Eigen::Vector3d &pattern, const CamModel &model)
      : image(image), pattern(pattern), model(model) {}

  template <typename T> bool operator()(const T *h, T *residuals) const {
    Eigen::Map<const Eigen::Matrix<T, 3, 3>> H(h);
    Eigen::Map<Eigen::Matrix<T, 2, 1>> res(residuals);
    Eigen::Matrix<T, 3, 1> ray = H * pattern.cast<T>();
    res = image.cast<T>() -
          model.project(ray, model.modelParams.template cast<T>().eval());
    return true;
  }

  Eigen::Vector2d image;
  Eigen::Vector3d pattern;
  const CamModel model;
  static ceres::CostFunction *create(const Eigen::Vector2d &image,
                                     const Eigen::Vector3d &pattern,
                                     const CamModel &model) {
    return (
        new ceres::AutoDiffCostFunction<HomographyModelCost<CamModel>, 2, 9>(
            new HomographyModelCost<CamModel>(image, pattern, model)));
  }
};

template <typename T>
std::pair<Eigen::Matrix3d, double>
nonLinearGomography(const T &model, const Eigen::Matrix3d &_H,
                    const FlatPattern &points) {

  Eigen::Matrix3d H = _H;
  ceres::Problem problem;
  auto S8_parameterization =
      new ceres::AutoDiffLocalParameterization<SNPlus<9>, 9, 8>;

  int Ntotal = points.size();
  for (int i = 0; i < points.size(); ++i) {
    auto homoCF = HomographyModelCost<T>::create(points.getPixel(i),
                                                 points.getPattern(i), model);

    problem.AddResidualBlock(homoCF, nullptr, H.data());
  }
  problem.AddParameterBlock(H.data(), 9, S8_parameterization);

  ceres::Solver::Options options;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = false;
  options.max_num_iterations = 10000;
  options.parameter_tolerance = 1e-10;
  options.gradient_tolerance = 1e-10;
  options.function_tolerance = 1e-10;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  LOG(INFO) << summary.BriefReport();

  double initial_err = sqrt(2 * summary.initial_cost / Ntotal);
  LOG(INFO) << "Homography (projection): INITIAL : " << initial_err;
  double final_err = sqrt(2 * summary.final_cost / Ntotal);
  LOG(INFO) << "Homography (projection): FINAL" << final_err;

  return {H, final_err};
}

Eigen::Matrix3d findHomography(const Eigen::Matrix<double, -1, 3> &left,
                               const Eigen::Matrix<double, -1, 3> &right);

#endif // HOMOGRAPHYCOMPUTATION_H

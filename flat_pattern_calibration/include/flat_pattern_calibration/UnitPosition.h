#ifndef SYSTEMSTRUCTS_H
#define SYSTEMSTRUCTS_H
#include <memory>
#include <sophus/se3.hpp>
#include <vector>
struct Constraint;

struct UnitPosition {
  UnitPosition() {}
  // this is constructor, when we want to fix position of smth
  UnitPosition(const Sophus::SE3d &pose) : pose(pose) {
    isFixed = true;
    known = true;
  }
  double *getSO3() { return pose.so3().data(); }
  double *getSE3() { return pose.data(); }
  Sophus::SE3d inverse() { return pose.inverse(); }
  void setPose(const Sophus::SE3d &p) {
    pose = p;
    known = true;
  }
  Sophus::SE3d pose;
  bool known = false;
  int sysID;
  std::vector<std::weak_ptr<Constraint>> connected;
  void addNeighbour(const std::shared_ptr<Constraint> &c) {
    connected.push_back(c);
  }
  bool isFixed = false;
  bool isFix() const { return isFixed; }
  virtual ~UnitPosition() {}
};

#endif // SYSTEMSTRUCTS_H

#include "camera_models/PinHoleCamera.h"
#include "camera_models/PinHoleCosts.h"
#include "camera_models/PinHoleLocalParametrization.h"
#include "flat_pattern_calibration/FlatPattern.h"
#include "flat_pattern_calibration/local_parametrization_se3.h"
#include <Eigen/Eigen>
#include <glog/logging.h>
#include <memory>
#include <sophus/se3.hpp>
#include <vector>
class PinHoleProblem {
public:
  PinHoleProblem(const std::shared_ptr<PinHoleModel> &model,
                 FlatPatterns &patterns)
      : model(model), patterns(patterns), modelParams(model->calibParams) {}

  void linearIntrinsicsSolve();
  void radialDistortionSolve();
  void tangentailDistortionSolve();
  bool focalEstimated = true;
  std::shared_ptr<PinHoleModel> model;
  FlatPatterns &patterns;
  PinHoleCalibrationParams modelParams;
  void decomposeIntrinsics(const Eigen::Matrix<double, 6, 1> &B);
  Sophus::SE3d bestSE3(const Sophus::SE3d &s1, const Sophus::SE3d &s2,
                       const FlatPattern &p);
};

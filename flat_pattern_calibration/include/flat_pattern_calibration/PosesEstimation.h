#ifndef POSESESTIMATION_H
#define POSESESTIMATION_H
#include "flat_pattern_calibration/FlatPattern.h"
#include "flat_pattern_calibration/HomographyComputation.h"
#include <Eigen/Eigen>
#include <camera_models/AtanFisheyeCamera.h>
#include <camera_models/OmnidirectionalCamera.h>
#include <camera_models/PinHoleCamera.h>
#include <ceres/ceres.h>
#include <sophus/se3.hpp>
#include <thread>

template <typename CamModel> struct PosesEstimationFunctor {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  PosesEstimationFunctor(const std::shared_ptr<CamModel> &model,
                         const Eigen::Vector2d &pixel,
                         const Eigen::Vector3d &pattern)
      : model(model), pixel(pixel), pattern(pattern) {}

  template <typename T>
  bool operator()(const T *euclidian, T *residuals) const {
    typedef Sophus::SE3<T> SE3T;
    Eigen::Map<const SE3T> se3(euclidian);
    Eigen::Map<Eigen::Matrix<T, 2, 1>> res(residuals);
    res = model->project(se3 * pattern.cast<T>(),
                         model->modelParams.template cast<T>().eval()) -
          pixel.cast<T>();
    return true;
  }

  static auto create(const std::shared_ptr<CamModel> &model,
                     const Eigen::Vector2d &pixel,
                     const Eigen::Vector3d &pattern) {
    auto cost = new ceres::AutoDiffCostFunction<PosesEstimationFunctor, 2, 7>(
        new PosesEstimationFunctor(model, pixel, pattern));
    return cost;
  }
  std::shared_ptr<CamModel> model;
  Eigen::Vector2d pixel;
  Eigen::Vector3d pattern;
};

template <typename CamModel> class OnlyPosesEstimation {
public:
  static double estimatePoses(const std::shared_ptr<CamModel> &model,
                              FlatPatterns &patterns) {
    // Get initial Homography
    double err = 0;
    for (auto &pattern : patterns) {
      Eigen::Matrix<double, 3, -1> goodImageRays(3, pattern->size());
      Eigen::Matrix<double, 3, -1> goodPatternRays(3, pattern->size());

      for (int j = 0; j < pattern->size(); ++j) {
        Eigen::Vector2d pixelPt = pattern->getPixel(j);
        goodImageRays.col(j) = model->projectBack(pixelPt);
        goodPatternRays.col(j) = pattern->getPattern(j);
      }

      auto H = findHomography(goodImageRays.transpose(),
                              goodPatternRays.transpose());

      auto Hpair = nonLinearGomography((*model), H, *pattern);

      H = Hpair.first;
      err += Hpair.second;

      Eigen::Matrix4d manSE3;
      manSE3.setZero();
      manSE3(3, 3) = 1;
      manSE3.col(0).head(3) = H.col(0).normalized();
      manSE3.col(1).head(3) = H.col(1).normalized();
      manSE3.col(2).head(3) =
          manSE3.col(0).head<3>().cross(manSE3.col(1).head<3>());
      manSE3.col(3).head(3) =
          2 * manSE3.col(2).head(3) / (H.col(0).norm() + H.col(1).norm());
      Eigen::Matrix4d manSE3_2 = -1 * manSE3;
      manSE3_2.col(2) *= -1;
      manSE3_2(3, 3) = 1;
      pattern->patternToCam =
          bestSE3(Sophus::SE3d::fitToSE3(manSE3),
                  Sophus::SE3d::fitToSE3(manSE3_2), model, *pattern);
    }
    return NonLinearPoses(model, patterns);
  }

  static double NonLinearPoses(const std::shared_ptr<CamModel> &model,
                               FlatPatterns &patterns) {
    ceres::Problem p;
    int Ntotal = 0;
    for (auto &pattern : patterns)
      for (auto &pt : pattern->Points) {
        auto cost = PosesEstimationFunctor<CamModel>::create(model, pt.first,
                                                             pt.second);
        p.AddResidualBlock(cost, nullptr, pattern->patternToCam.data());
        Ntotal++;
      }

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout = false;
    options.num_threads = std::thread::hardware_concurrency();
    options.max_num_iterations = 1000;
    options.parameter_tolerance = 1e-16;
    options.gradient_tolerance = 1e-16;
    options.function_tolerance = 1e-16;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &p, &summary);
    LOG(INFO) << summary.FullReport();

    LOG(INFO) << "INITIAL : " << sqrt(2 * summary.initial_cost / Ntotal);

    double final_error = sqrt(2 * summary.final_cost / Ntotal);
    LOG(INFO) << "FINAL : " << final_error;
    return final_error;
  }

  static Sophus::SE3d bestSE3(const Sophus::SE3d &s1, const Sophus::SE3d &s2,
                              const std::shared_ptr<CamModel> &model,
                              const FlatPattern &p) {
    int s1N = 0, s2N = 0;
    for (int i = 0; i < p.size(); ++i) {
      Eigen::Vector2d pt = p.getPixel(i);
      Eigen::Vector3d ray = model->projectBack(pt);
      if (ray.dot(s1 * p.getPattern(i)) > 0)
        s1N++;
      if (ray.dot(s2 * p.getPattern(i)) > 0)
        s2N++;
    }
    if (s1N > s2N)
      return s1;
    else
      return s2;
  }
};
#endif // POSESESTIMATION_H

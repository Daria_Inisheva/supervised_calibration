#ifndef FLATPATTERN_H
#define FLATPATTERN_H
#include <Eigen/Eigen>
#include <flat_pattern_calibration/UnitPosition.h>
#include <memory>
#include <sophus/se3.hpp>
#include <string>
#include <vector>

struct Pattern : public UnitPosition {
  std::string id;
  Pattern(const Sophus::SE3d &pose) : UnitPosition(pose) {}
  Pattern() : UnitPosition() {}
};

struct FlatPattern {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  typedef std::pair<Eigen::Vector2d, Eigen::Vector3d> PatternPair;
  typedef std::vector<PatternPair, Eigen::aligned_allocator<PatternPair>>
      PatternVector;
  PatternVector Points;

  FlatPattern() {}
  FlatPattern(const std::shared_ptr<Pattern> &pattern) : pattern(pattern) {}
  FlatPattern(const std::shared_ptr<Pattern> &pattern,
              const PatternVector &Points)
      : Points(Points), pattern(pattern) {}

  FlatPattern(const std::shared_ptr<Pattern> &pattern,
              const PatternVector &Points, const Sophus::SE3d &patternToCam)
      : Points(Points), pattern(pattern), patternToCam(patternToCam) {}

  std::shared_ptr<Pattern> pattern;

  void addPair(const Eigen::Vector2d &point, const Eigen::Vector3d &pattern) {
    Points.push_back({point, pattern});
  }

  void addPair(const PatternPair &p) { Points.push_back(p); }

  Sophus::SE3d patternToCam;
  Eigen::Vector2d getPixel(const int &i) const {
    assert(i < size());
    return Points[i].first;
  }
  Eigen::Vector3d getPattern(const int &i) const {
    assert(i < size());
    return Points[i].second;
  }
  int size() const { return Points.size(); }
  std::string t;
};

typedef std::vector<std::shared_ptr<FlatPattern>> FlatPatterns;

#endif /* FLATPATTERN_H */

#ifndef ATANFISHEYECALIBRATION_H
#define ATANFISHEYECALIBRATION_H

#include "FlatPattern.h"
#include "camera_models/AtanFisheyeCamera.h"
#include "camera_models/AtanFisheyeCosts.h"
#include "camera_models/AtanFisheyeParametrization.h"
#include "flat_pattern_calibration/FlatPattern.h"
#include "flat_pattern_calibration/HomographyComputation.h"
#include "flat_pattern_calibration/PosesEstimation.h"
#include "flat_pattern_calibration/local_parametrization_se3.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>
#include <glog/logging.h>
#include <memory>
#include <vector>

class AtanFisheyeCalibration {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  explicit AtanFisheyeCalibration(
      const std::shared_ptr<AtanFisheyeModel> &model, FlatPatterns &patterns)
      : model(model), patterns(patterns) {
    LOG(INFO) << "Atan calibration class created";
    LOG(INFO) << "model params " << model->modelParams.transpose();
    LOG(INFO) << "Pattern n " << patterns.size();
  }

  std::shared_ptr<AtanFisheyeModel> model;
  FlatPatterns &patterns;
  ceres::LossFunction *loss = nullptr;

  void setLoss(ceres::LossFunction *loss_ = new ceres::CauchyLoss(2.0)) {
    loss = loss_;
  }

  double calibrate();

private:
  double globalNNOptimization(ceres::LossFunction *loss_ = nullptr);
  double homographyPosesEstimation();
  Sophus::SE3d bestSE3(const Sophus::SE3d &s1, const Sophus::SE3d &s2,
                       const FlatPattern &patt);
};

#endif // ATANFISHEYECALIBRATION_H

#include "FlatPatternGenerator.h"
#include <flat_pattern_calibration/OmnidirectionalCalibration.h>
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
#include <memory>
#include <random>
#include <sophus/se3.hpp>
#include <string>

const int PatternCount = 20;

typedef Eigen::Vector3d V3;
typedef Eigen::Vector2d V2;

class OmnidirectionalCalibrationTest : public ::testing::Test {
protected:
  void SetUp() {
    Eigen::Matrix2d stretchMatrix = Eigen::Matrix2d::Identity();
    Eigen::Vector2d principal(2000, 1800);
    Eigen::VectorXd coeffs(3);
    coeffs(0) = 2000.549;
    coeffs(1) = -0.000233596;
    coeffs(2) = -0.0000000125904;

    model = new OmnidirectionalModel(stretchMatrix, principal, coeffs);
    model->print();
    std::uniform_real_distribution<double> runif(-20, 20);
    std::uniform_real_distribution<double> angle(-1, 1);
    std::mt19937 rng;

    for (int i = 0; i < PatternCount; ++i) {
      V3 T(runif(rng), runif(rng), runif(rng));
      Eigen::Matrix3d R;
      R = Sophus::SO3d::exp(V3(angle(rng), angle(rng), angle(rng))).matrix();
      try {
        auto currPattern =
            flatPatternGenerator(*model, 4000, 3500, 6, 6, R, T, 5);
        pattern.push_back(currPattern);
      } catch (...) {
        // std::cout << "Failed to project pattern to camera..." << std::endl;
        --i;
      }
    }
  }
  void TearDown() { delete model; }
  OmnidirectionalModel *model;
  FlatPatterns pattern;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

TEST_F(OmnidirectionalCalibrationTest, ChessPattern) {
  OmnidirectionalCalibrationParams params(5);
  std::shared_ptr<OmnidirectionalModel> testModel(
      new OmnidirectionalModel(4000, 3500, params));
  OmnidirectionalCalibration calib(testModel, pattern);
  double error = calib.calibrate();

  std::cout << "old params : \n";
  model->print();
  std::cout << "\n new params : \n";
  testModel->print();
  EXPECT_GE(1, error);
}

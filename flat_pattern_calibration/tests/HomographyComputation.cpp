#include "flat_pattern_calibration/HomographyComputation.h"
#include <gtest/gtest.h>
#include <iostream>
#include <random>

const int N = 100;

class HomographyTest : public ::testing::Test {
protected:
  void SetUp() {
    std::uniform_real_distribution<double> runif(-5, 5);
    std::mt19937 rng;
    right = Eigen::Matrix<double, -1, 3>(N, 3);
    H_0 << 2.0, 3.0, 1.0, -1.0, 2.0, 0.5, 0.001, 0.001, 1.0;
    left = Eigen::Matrix<double, -1, 3>(N, 3);
    for (int i = 0; i < N; ++i) {
      Eigen::Matrix<double, 1, 3> pt(runif(rng), runif(rng), 1);
      right.row(i) = pt;
      decltype(pt) noise =
          Eigen::Vector3d(runif(rng) / 1e3, runif(rng) / 1e3, 0).transpose();
      left.row(i) = (pt * H_0.transpose() + noise);
    }
  }

  void TearDown() {}
  Eigen::Matrix<double, 3, 3> H_0;
  Eigen::Matrix<double, -1, 3> left, right;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

TEST_F(HomographyTest, KnownHomoTest) {
  auto H = findHomography(left, right);

  double norm_sum = (H.normalized() - H_0.normalized()).norm();
  double norm_min = (H.normalized() + H_0.normalized()).norm();

  std::cout << "Relative diff after NNLS:" << std::min(norm_min, norm_sum)
            << std::endl;
  EXPECT_GE(1e-2, std::min(norm_min, norm_sum));
}

TEST_F(HomographyTest, KnownHomoTestDLT) {
  auto H = computeHomographyDLT(left, right);
  std::cout << "DLT : " << H << std::endl;
  double norm_sum = (H.normalized() - H_0.normalized()).norm();
  double norm_min = (H.normalized() + H_0.normalized()).norm();

  std::cout << "Relative diff after DLT:" << std::min(norm_min, norm_sum)
            << std::endl;
  EXPECT_GE(1e-2, std::min(norm_min, norm_sum));
}

TEST_F(HomographyTest, Identity) {
  auto H = computeHomographyDLT(left, left);
  Eigen::Matrix3d i = Eigen::Matrix3d::Identity();
  double norm_sum = (H.normalized() - i.normalized()).norm();
  double norm_min = (H.normalized() + i.normalized()).norm();

  EXPECT_GE(1e-10, std::min(norm_min, norm_sum));
}

const int Num_BoxTests = 100;

TEST(LocalParam, BoxPlusNull) {
  for (int i = 0; i < Num_BoxTests; ++i) {
    Eigen::Matrix<double, 9, 1> f;
    std::uniform_real_distribution<double> runif(-10, 10);
    std::mt19937 rng;
    for (int i = 0; i < 9; ++i)
      f(i) = runif(rng);
    f.normalize();
    Eigen::Matrix<double, 8, 1> delta;
    delta.setZero();
    Eigen::Matrix<double, 9, 1> Plus;
    SNPlus<9> s;
    s.operator()(f.data(), delta.data(), Plus.data());
    EXPECT_GE(1e-10, (f - Plus).norm());
  }
}

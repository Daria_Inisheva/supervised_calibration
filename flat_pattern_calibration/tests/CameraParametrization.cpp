#include <gtest/gtest.h>
#include <iostream>
#include <random>

#include <camera_models/AtanFisheyeParametrization.h>
#include <camera_models/OmnidirectionalParametrization.h>
#include <camera_models/PinHoleLocalParametrization.h>

TEST(OmnidirectionalParametrization, fixedStretch) {
  Eigen::VectorXd params(5 + 3);
  params << 9, 0, 0, 100, 100, 1, 2, 3;
  OmnidirectionalCalibrationParams P;
  P.fixedStretch = true;
  P.rN = 3;
  P.recalculateSizes();
  auto local = LocalParameterizationOmnidir::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  EXPECT_EQ(params[0], out[0]);
  delete local;
}

TEST(OmnidirectionalParametrization, fixedPolynom) {
  Eigen::VectorXd params(5 + 3);
  params << 9, 0, 0, 100, 100, 1, 2, 3;
  OmnidirectionalCalibrationParams P;
  P.fixedPolynom = true;
  P.rN = 3;
  P.recalculateSizes();
  auto local = LocalParameterizationOmnidir::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());

  out.tail(3) -= params.tail(3);

  EXPECT_EQ(0, out.tail(3).norm());
  delete local;
}

TEST(OmnidirectionalParametrization, ExpectedChange) {
  Eigen::VectorXd params(5 + 3);
  params << 9, 0, 0, 100, 100, 1, 2, 3;
  OmnidirectionalCalibrationParams P;
  P.rN = 3;
  P.recalculateSizes();
  auto local = LocalParameterizationOmnidir::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  params += delta;
  EXPECT_EQ(0, (out - params).squaredNorm());
  delete local;
}

TEST(PinHileParametrization, fixedFocal) {
  Eigen::VectorXd params(5 + 3 + 2);
  params << 1, 2, 3, 4, 0.5, 10, 20, 30, 90, 200;
  PinHoleCalibrationParams P;
  P.fixedFocal = true;
  P.rN = 3;
  P.tN = 2;
  P.recalculateSizes();
  auto local = LocalParameterizationPinHole::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  EXPECT_EQ(0, (params.head(2) - out.head(2)).norm());
  delete local;
}

TEST(PinHileParametrization, equalFocal) {
  Eigen::VectorXd params(5 + 3 + 2);
  params << 1, 1, 3, 4, 0.5, 10, 20, 30, 90, 200;
  PinHoleCalibrationParams P;
  P.equalFocal = true;
  P.rN = 3;
  P.tN = 2;
  P.recalculateSizes();
  auto local = LocalParameterizationPinHole::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  EXPECT_EQ(0, (out(1) - out(0)));
  delete local;
}

TEST(PinHileParametrization, fixedRadial) {
  Eigen::VectorXd params(5 + 3 + 2);
  params << 1, 2, 3, 4, 0.5, 10, 20, 30, 90, 200;
  PinHoleCalibrationParams P;
  P.fixedRadial = true;
  P.rN = 3;
  P.tN = 2;
  P.recalculateSizes();
  auto local = LocalParameterizationPinHole::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  EXPECT_EQ(0, (params.head(5 + 3).tail(3) - out.head(5 + 3).tail(3)).norm());
  delete local;
}

TEST(PinHileParametrization, fixedTangential) {
  Eigen::VectorXd params(5 + 3 + 2);
  params << 1, 2, 3, 4, 0.5, 10, 20, 30, 90, 200;
  PinHoleCalibrationParams P;
  P.fixedTangential = true;
  P.rN = 3;
  P.tN = 2;
  P.recalculateSizes();
  auto local = LocalParameterizationPinHole::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  EXPECT_EQ(0, (params.tail(2) - out.tail(2)).norm());
  delete local;
}

TEST(PinHileParametrization, fixMainTangential) {
  Eigen::VectorXd params(5 + 3 + 4);
  params << 1, 2, 3, 4, 0.5, 10, 20, 30, 90, 200, 1000, 9;
  PinHoleCalibrationParams P;
  P.fixMainTangential = true;
  P.rN = 3;
  P.tN = 4;
  P.recalculateSizes();
  auto local = LocalParameterizationPinHole::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  EXPECT_EQ(0, (params.tail(4).head(2) - out.tail(4).head(2)).norm());
  delete local;
}

TEST(PinHileParametrization, UxpectedChange) {
  Eigen::VectorXd params(5 + 3 + 4);
  params << 1, 2, 3, 4, 0.5, 10, 20, 30, 90, 200, 1000, 9;
  PinHoleCalibrationParams P;
  P.rN = 3;
  P.tN = 4;
  P.recalculateSizes();
  auto local = LocalParameterizationPinHole::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  params += delta;
  EXPECT_EQ(0, (params - out).norm());
  delete local;
}

TEST(PinHileParametrization, OddRadial) {
  Eigen::VectorXd params(5 + 3 + 4);
  params << 1, 2, 3, 4, 0.5, 10, 20, 30, 90, 200, 1000, 9;
  PinHoleCalibrationParams P;
  P.fixOddRadial = true;
  P.rN = 3;
  P.tN = 4;
  P.recalculateSizes();
  auto local = LocalParameterizationPinHole::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  double ans = 0;
  for (int i = 5; i < 5 + 3; i += 2)
    ans += params[i] - out[i];
  EXPECT_EQ(0, ans);
  delete local;
}

TEST(AtanFisheyeParametrization, fixedFocal) {
  Eigen::VectorXd params(5 + 3);
  params << 9, 5, 3, 100, 100, 1, 2, 3;
  AtanCalibrationParams P;
  P.fixedFocal = true;
  P.rN = 3;
  P.recalculateSizes();
  auto local = LocalParameterizationAtan::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());

  out.head(2) -= params.head(2);

  EXPECT_EQ(0, out.head(2).norm());
  delete local;
}

TEST(AtanFisheyeParametrization, fixedPolynom) {
  Eigen::VectorXd params(5 + 3);
  params << 9, 0, 0, 100, 100, 1, 2, 3;
  AtanCalibrationParams P;
  P.fixedPolynom = true;
  P.rN = 3;
  P.recalculateSizes();
  auto local = LocalParameterizationAtan::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());

  out.tail(3) -= params.tail(3);

  EXPECT_EQ(0, out.tail(3).norm());
  delete local;
}

TEST(AtanFisheyeParametrization, equalFocal) {
  Eigen::VectorXd params(5 + 3);
  params << 9, 9, 0, 100, 100, 1, 2, 3;
  AtanCalibrationParams P;
  P.equalFocal = true;
  P.rN = 3;
  P.recalculateSizes();
  auto local = LocalParameterizationAtan::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());

  out.tail(3) -= params.tail(3);

  EXPECT_EQ(out[0], out[1]);
  delete local;
}

TEST(AtanFisheyeParametrization, fixedSkew) {
  Eigen::VectorXd params(5 + 3);
  params << 9, 9, 0, 100, 100, 1, 2, 3;
  AtanCalibrationParams P;
  P.fixedSkew = true;
  P.rN = 3;
  P.recalculateSizes();
  auto local = LocalParameterizationAtan::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());

  EXPECT_EQ(params[4], out[4]);
  delete local;
}

TEST(AtanFisheyeParametrization, dixOddPolynom) {
  Eigen::VectorXd params(5 + 4);
  params << 9, 0, 0, 100, 100, 1, 2, 3, 10;
  AtanCalibrationParams P;
  P.fixOdd = true;
  P.rN = 3;
  P.recalculateSizes();
  auto local = LocalParameterizationAtan::create(P);
  Eigen::VectorXd out(params.rows());
  std::uniform_real_distribution<double> angle(-1, 1);
  std::mt19937 rng;
  Eigen::VectorXd delta(P.lS);
  for (int i = 0; i < delta.rows(); ++i)
    delta[i] = angle(rng);
  local->Plus(params.data(), delta.data(), out.data());
  double ans = 0;
  for (int i = 5; i < 5 + 4; i += 2)
    ans += out[i] - params[i];

  EXPECT_EQ(0, ans);
  delete local;
}

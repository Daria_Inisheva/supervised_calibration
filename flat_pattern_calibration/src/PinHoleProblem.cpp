#include "flat_pattern_calibration/PinHoleProblem.h"
#include "flat_pattern_calibration/HomographyComputation.h"
#include <sophus/se3.hpp>
#include <thread>

Sophus::SE3d PinHoleProblem::bestSE3(const Sophus::SE3d &s1,
                                     const Sophus::SE3d &s2,
                                     const FlatPattern &p) {
  int s1N = 0, s2N = 0;
  for (int i = 0; i < p.size(); ++i) {
    Eigen::Vector2d pt = p.getPixel(i);
    Eigen::Vector3d ray = model->projectBack(pt);
    Eigen::Vector3d pattern = p.getPattern(i);
    if (ray.dot(s1 * pattern) > 0)
      s1N++;
    if (ray.dot(s2 * pattern) > 0)
      s2N++;
  }
  if (s1N > s2N)
    return s1;
  else
    return s2;
}

void PinHoleProblem::decomposeIntrinsics(const Eigen::Matrix<double, 6, 1> &B) {
  Eigen::Vector2d principal, focal;
  double skew;
  double principal_nom = (B(1) * B(3) - B(0) * B(4));
  double principal_denom = (B(0) * B(2) - B(1) * B(1));

  principal(1) = principal_nom / principal_denom;
  double alpha = (B(1) * B(3) - B(0) * B(4));
  double la_1 = (B(3) * B(3) + principal(1) * alpha);
  double la = B(5) - la_1 / B(0);

  focal(0) = (la / B(0));
  focal(1) = (la * B(0) / (B(0) * B(2) - B(1) * B(1)));

  if (focal(1) < 0 || focal(0) < 0) {
    focalEstimated = false;
    principal = Eigen::Vector2d(model->width / 2, model->height / 2);
    focal = Eigen::Vector2d(model->width, model->height);
    skew = 0;
  } else {
    focalEstimated = true;
    focal(0) = sqrt(focal(0));
    focal(1) = sqrt(focal(1));
    skew = B(1) * focal(0) * focal(1) / la;
    double principal_0 = skew * principal(1) / focal(1);
    double principal_1 = B(3) * focal(0) * focal(0) / la;
    principal(0) = principal_0 - principal_1;
  }

  LOG(INFO) << "computed Focal " << focal.transpose() << " principal "
            << principal.transpose() << " skew " << skew;

  if (!modelParams.fixedFocal) {
    model->modelParams.head(2) = focal;
    if (modelParams.equalFocal) {
      model->modelParams(0) = (focal(0) + focal(1)) / 2;
      model->modelParams(1) = model->modelParams(0);
    }
  }
  if (!modelParams.fixedPrincipal) {
    model->modelParams[2] = principal(0);
    model->modelParams[3] = principal(1);
    if (modelParams.equalPrincipal) {
      model->modelParams(2) = (principal(0) + principal(1)) / 2;
      model->modelParams(3) = model->modelParams(2);
    }
  }
  if (!modelParams.fixedSkew && !modelParams.zeroSkew)
    model->modelParams[4] = skew;

  if (modelParams.zeroSkew)
    model->modelParams[4] = 0;
}

// this function find initial guess of focal, principal, skew and SE3
void PinHoleProblem::linearIntrinsicsSolve() {
  // This is first part of the calibration
  // Finding homographies for all observed patterns.
  //
  // v_pq is (96)
  LOG(INFO) << "LinearEstimation start";
  LOG(INFO) << "Pattern N " << patterns.size();
  LOG(INFO) << "ModelParams " << model->modelParams.transpose();
  auto v_pq = [](int p, int q, auto h) {
    Eigen::Matrix<double, 6, 1> v;
    v << h(0, p) * h(0, q), h(0, p) * h(1, q) + h(1, p) * h(0, q),
        h(1, p) * h(1, q), h(2, p) * h(0, q) + h(0, p) * h(2, q),
        h(2, p) * h(1, q) + h(1, p) * h(2, q), h(2, p) * h(2, q);
    return v;
  };

  std::vector<Eigen::Matrix3d, Eigen::aligned_allocator<Eigen::Matrix3d>>
      homoghrphies;
  Eigen::Matrix<double, -1, 6> A(2 * patterns.size(), 6);

  for (size_t i = 0; i < patterns.size(); ++i) {
    int ptsN = patterns[i]->size();

    LOG(INFO) << "Points N " << ptsN;
    Eigen::Matrix<double, 3, -1> observedPts(3, ptsN);
    Eigen::Matrix<double, 3, -1> observedPatterns(3, ptsN);
    for (int j = 0; j < ptsN; ++j) {
      Eigen::Vector2d pix = patterns[i]->getPixel(j);
      if (!focalEstimated) {
        pix = model->projectBack(pix).hnormalized();
        pix(0) = model->modelParams(0) * pix(0) + model->modelParams(2);
        pix(1) = model->modelParams(1) * pix(1) + model->modelParams(3);
      }
      observedPts.col(j) = pix.homogeneous();
      observedPatterns.col(j) = patterns[i]->getPattern(j);
    }

    auto H =
        findHomography(observedPts.transpose(), observedPatterns.transpose());
    homoghrphies.push_back(H);
    A.row(i * 2) = v_pq(0, 1, H).transpose();
    A.row(i * 2 + 1) = (v_pq(0, 0, H) - v_pq(1, 1, H)).transpose();
  }

  LOG(INFO) << "Start of linear instrinsic estimation";
  auto svd = A.jacobiSvd(Eigen::ComputeFullV).matrixV();

  Eigen::Matrix<double, 6, 1> ans = svd.col(svd.cols() - 1);
  // extra values needed to compute intrinsics (104), (105)
  // computation (99 - 103)

  decomposeIntrinsics(ans);

  if (!focalEstimated)
    return;
  LOG(INFO) << "ModelParams after estimation" << model->modelParams.transpose();

  ceres::Problem p;
  int Ntotal = 0;
  // extrinsic params estimation:
  Eigen::Matrix3d K = model->getCalibMatrix();
  Eigen::Matrix3d Kinv = K.inverse();
  LOG(INFO) << "NonLinear intrinsic pattern N" << patterns.size();
  for (size_t i = 0; i < patterns.size(); ++i) {
    Eigen::Vector3d r0 = (Kinv * homoghrphies[i].col(0));
    Eigen::Vector3d r1 = (Kinv * homoghrphies[i].col(1));
    Eigen::Vector3d t =
        2 * (Kinv * homoghrphies[i].col(2)) / (r0.norm() + r1.norm());
    r0.normalize();
    r1.normalize();
    auto r2 = r0.cross(r1);

    Eigen::Matrix4d motion = Eigen::Matrix4d::Zero();
    motion.col(0).head(3) = r0;
    motion.col(1).head(3) = r1;
    motion.col(2).head(3) = r2;
    motion.col(3).head(3) = t;
    motion(3, 3) = 1;
    Eigen::Matrix4d motion1 = (-1) * motion;
    motion1.col(2) *= -1;
    motion1(3, 3) = 1;

    patterns[i]->patternToCam =
        bestSE3(Sophus::SE3d::fitToSE3(motion), Sophus::SE3d::fitToSE3(motion1),
                *patterns[i]);

    p.AddParameterBlock(patterns[i]->patternToCam.data(),
                        Sophus::SE3d::num_parameters,
                        new Sophus::LocalParameterizationSE3);

    Ntotal += (patterns)[i]->size();
    for (int k = 0; k < patterns[i]->size(); ++k) {

      auto homoCF = pinhole_costs::PinHoleCalibrationFunctor::create(
          patterns[i]->getPixel(k), patterns[i]->getPattern(k), model->radialN,
          model->tangentialN);

      p.AddResidualBlock(homoCF, nullptr, model->modelParams.data(),
                         patterns[i]->patternToCam.data());
    }
  }

  auto linearParams = modelParams;
  linearParams.fixedTangential = true;
  linearParams.fixedRadial = true;
  linearParams.recalculateSizes();
  p.AddParameterBlock(model->modelParams.data(), model->modelParams.rows(),
                      model->getLocalParametrization(&linearParams));

  // Maximum likehood estimation from Zhang`s article
  // 3.2
  ceres::Solver::Options options;
  options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = false;
  options.num_threads = std::thread::hardware_concurrency();
  options.max_num_iterations = 1000;
  options.parameter_tolerance = 1e-16;
  options.gradient_tolerance = 1e-16;
  options.function_tolerance = 1e-16;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &p, &summary);

  LOG(INFO) << summary.FullReport();

  double initial_error = sqrt(2 * summary.initial_cost / Ntotal);

  LOG(INFO) << "Initial error = " << initial_error;
  double final_error = sqrt(2 * summary.final_cost / Ntotal);
  LOG(INFO) << "Final error = " << final_error;
}

void PinHoleProblem::radialDistortionSolve() {
  // this is linear estimation of radial distortion
  // in normalized image coords
  // -xr -xr^2 -xr^3 ... * k = x-u
  // -yr -yr^2 -yr^3 ... * k = y-v
  LOG(INFO) << "Radial distortion estimation pattern N " << patterns.size();
  int ptsN = 0;
  for (size_t i = 0; i < patterns.size(); ++i)
    ptsN += patterns[i]->size();

  LOG(INFO) << "All points size = " << ptsN;
  if (modelParams.rN != 0 && !modelParams.fixedRadial &&
      (!modelParams.fixOddRadial || !modelParams.fixEvenRadial)) {
    int radialCoeffsN = 0;
    if (!modelParams.fixOddRadial)
      radialCoeffsN += model->radialN / 2 + model->radialN % 2;
    if (!modelParams.fixEvenRadial)
      radialCoeffsN += model->radialN / 2;

    Eigen::Matrix<double, -1, -1> A(2 * ptsN, radialCoeffsN);
    Eigen::VectorXd B(2 * ptsN);
    int inargcount = 0;
    for (auto pattern : patterns) {
      for (int j = 0; j < pattern->size(); ++j) {
        Eigen::Vector2d u = pattern->getPixel(j);
        Eigen::Vector2d x =
            (pattern->patternToCam * pattern->getPattern(j)).hnormalized();
        double r = x.norm();

        Eigen::Vector2d diff = x - model->projectBack(u).hnormalized();

        B(inargcount) = diff(0);
        B(inargcount + 1) = diff(1);
        double r_step = r;
        radialCoeffsN = 0;
        for (int k = 0; k < model->radialN; ++k) {

          if (!modelParams.fixEvenRadial && (1 + k) % 2 == 0) {
            A(inargcount, radialCoeffsN) = -x(0) * r_step;
            A(inargcount + 1, radialCoeffsN++) = -x(1) * r_step;
          }

          if (!modelParams.fixOddRadial && (1 + k) % 2 == 1) {
            A(inargcount, radialCoeffsN) = -x(0) * r_step;
            A(inargcount + 1, radialCoeffsN++) = -x(1) * r_step;
          }

          r_step *= r;
        }
        inargcount += 2;
      }
    }
    Eigen::VectorXd radialCoeffs = A.colPivHouseholderQr().solve(B);
    int radCoeffs = 0;
    LOG(INFO) << "Estimated radial = " << radialCoeffs.transpose();
    for (int k = 0; k < model->radialN; ++k) {
      model->modelParams[5 + k] = 0;
      if (!modelParams.fixEvenRadial && (1 + k) % 2 == 0)
        model->modelParams[5 + k] = radialCoeffs[radCoeffs++];
      if (!modelParams.fixOddRadial && (1 + k) % 2 == 1)
        model->modelParams[5 + k] = radialCoeffs[radCoeffs++];
    }
  }
}
void PinHoleProblem::tangentailDistortionSolve() {

  LOG(INFO) << "Tangential distortion";
  // this is linear estimation of first 2 coeffs
  // of tangential distortion
  // (2xy) + (r^2 + 2 x^2)    (a1)     =    u - x
  // (r^2 + 2 y^2) + (2xy)    (a2)     =    v - y
  if (modelParams.tN != 0 && !modelParams.fixedTangential &&
      !modelParams.fixMainTangential) {
    int ptsN = 0;
    for (size_t i = 0; i < patterns.size(); ++i)
      ptsN += (patterns)[i]->size();

    int inargcount = 0;
    Eigen::Matrix<double, -1, -1> A(2 * ptsN, 2);
    Eigen::VectorXd B(2 * ptsN);

    for (auto pattern : patterns) {
      for (int j = 0; j < pattern->size(); ++j) {
        Eigen::Vector2d u = pattern->getPixel(j);
        Eigen::Vector2d x =
            (pattern->patternToCam * pattern->getPattern(j)).hnormalized();
        double r = x.norm();

        Eigen::Vector2d diff;
        diff = x - model->projectBack(u).hnormalized();

        B(inargcount) = diff(0);

        B(inargcount + 1) = diff(1);

        A(inargcount, 0) = 2 * x(0) * x(1);
        A(inargcount, 1) = r * r + 2 * x(0) * x(0);

        A(inargcount + 1, 0) = r * r + 2 * x(1) * x(1);
        A(inargcount + 1, 1) = 2 * x(0) * x(1);

        inargcount += 2;
      }
    }
    Eigen::Vector2d tangential;
    tangential = A.colPivHouseholderQr().solve(B);
    if (!std::isfinite(tangential(0)))
      tangential(0) = 0;
    if (!std::isfinite(tangential(1)))
      tangential(1) = 0;
    LOG(INFO) << "Estimated tangential " << tangential.transpose();
    this->model->modelParams(5 + this->model->radialN) = tangential(0);
    this->model->modelParams(5 + this->model->radialN + 1) = tangential(1);
  }
}

#include "flat_pattern_calibration/OmnidirectionalCalibration.h"
#include "flat_pattern_calibration/HomographyComputation.h"
#include "flat_pattern_calibration/local_parametrization_se3.h"

#include <sophus/se3.hpp>
#include <stdint.h>
#include <thread>

double OmnidirectionalCalibration::calibrate() {
  if (model->calibParams.isfixed()) {
    double err = OnlyPosesEstimation<OmnidirectionalModel>::estimatePoses(
        model, patterns);
    LOG(INFO)
        << "Only estimating poses, camera is supposed to ba calibrated error = "
        << err;
    return err;
  }

  double linError = linearEstimation();
  LOG(INFO) << "Initial error after linear solving : " << linError;

  LOG(INFO) << "Start NonLinearGlobalOptimization";
  double error = globalNNOptimization();
  LOG(INFO) << "Calibration Error (no Loss) = " << error;
  if (loss != nullptr) {
    error = globalNNOptimization(loss);
    LOG(INFO) << "Calibration Error (with Loss) = " << error;
  }
  return error;
}

double OmnidirectionalCalibration::linearEstimation() {

  OmnidirectionalProblem::OmnidirectionalParams modelParams(
      model->getDegree(), model->width, model->height);
  OmnidirectionalProblem calibrationProblem(modelParams, patterns);

  calibrationProblem.solveParameters(modelParams);

  model->convertModel(modelParams.scale, modelParams.center,
                      modelParams.polyCoeffs);

  OmnidirectionalCalibrationParams params = model->calibParams;

  if (params.equalPrincipal) {
    model->modelParams[2] = (model->modelParams[3] + model->modelParams[2]) / 2;
    model->modelParams[3] = model->modelParams[2];
  }

  if (params.diagonalStretch || params.identStretch) {
    model->modelParams[1] = 0;
    if (params.identStretch)
      model->modelParams[0] = 1;
  }

  // Optimization on pattern plane
  double patternPlaneError = calibrationProblem.patternPlaneOptimization(model);
  LOG(INFO) << "\n Optimizing on pattern plane error : " << patternPlaneError;

  for (size_t i = 0; i < patterns.size(); ++i) {
    patterns[i]->patternToCam = calibrationProblem.patterns[i].patternToCam;
  }

  double error = calibrationProblem.computeRMSE(model);

  return error;
}

double
OmnidirectionalCalibration::globalNNOptimization(ceres::LossFunction *loss_) {
  int Ntotal = 0;
  // Initialize Problem with localparam, model fields and etc
  problem.AddParameterBlock(model->modelParams.data(),
                            model->modelParams.rows(),
                            model->createLocalParametrization());
  model->calibParams.recalculate();

  for (auto &pattern : patterns) {

    problem.AddParameterBlock(pattern->patternToCam.data(),
                              Sophus::SE3d::num_parameters,
                              new Sophus::LocalParameterizationSE3);

    Ntotal += pattern->size();
    for (int k = 0; k < pattern->size(); ++k) {

      auto homoCF = omnidirectional_costs::OmnidirectionalFunctor::create(
          pattern->getPixel(k), pattern->getPattern(k),
          model->modelParams.rows());

      problem.AddResidualBlock(homoCF, loss_, model->modelParams.data(),
                               pattern->patternToCam.data());
    }
  }

  double final_error = 0;
  ceres::Solver::Options options;
  options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
  options.minimizer_progress_to_stdout = false;
  options.num_threads = std::thread::hardware_concurrency();
  options.max_num_iterations = 1000;
  options.parameter_tolerance = 1e-16;
  options.gradient_tolerance = 1e-16;
  options.function_tolerance = 1e-16;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  LOG(INFO) << summary.FullReport();

  LOG(INFO) << "INITIAL : " << sqrt(2 * summary.initial_cost / Ntotal);

  final_error = sqrt(2 * summary.final_cost / Ntotal);
  LOG(INFO) << "FINAL : " << final_error;

  return final_error;
}

#!/usr/bin/python3
"""

THIS IS AN EXAMPLE OF USING PYTHON WRAPPER TO 
CALIBRTE SINGLE CAMER MODEL FROM DUMP

"""
import sys
sys.path.insert(0, "../../build/wrappers/")

import super_calibration as sc
import numpy as np
from read_file import read_dump

if(len(sys.argv) != 2):
    print("You should pass valid path to DUMP")
    exit(1)
# reading dump name from command line arguments
dump_name = sys.argv[1]

# Reading dump W and H are important 
fps, w, h = read_dump(dump_name)

# creating camera model with all needed params
cam = sc.createCamera("Atan-fisheye", w, h, polynom_n=5, equal_focal=True)


error = sc.calibrateCamera(cam, fps, with_loss=True)
print("calibration error : ", error)
cam.print()

#cam.exportToOpencv("atan.yml")


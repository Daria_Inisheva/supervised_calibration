
.. highlight:: c++

.. default-domain:: cpp

.. _chapter-omnidirectional_model:


Omnidirectinal model
====================
Omnidirectional model is same as in matlab_ , but size of polynom is not fixed to be 4.

.. _matlab: https://www.mathworks.com/help/vision/ug/fisheye-calibration-basics.html

This model is described by *Stretch matrix*, principal point and polynomial.

Stretch matrix
--------------

Stretch matrix looks like

.. math::

  S = \begin{pmatrix} c & d \\  e & 1    \end{pmatrix}, 


Polynomial
----------

This polynomial is functon wich depends only on the distance of the point from the principal point.

.. math::

  \rho (r) = \sum_{i=0}^n a_ir^i

Proejction
----------

Projection from pixel point to ray works like this: 

.. math::

  \begin{equation}
  \begin{pmatrix} u' \\ v' \end{pmatrix} = 
  Stretch^{-1} \left(\begin{pmatrix} u \\ v \end{pmatrix}  -
  \begin{pmatrix} p_x \\ p_y \end{pmatrix}\right) \\
  r_0 = \sqrt{{u'}^2 + {b'}^2} \\
  \begin{pmatrix} X \\ Y \\ Z \end{pmatrix} = \lambda
  \begin{pmatrix} u' \\ v' \\ \rho(r_0) \end{pmatrix}
  \end{equation},

where :math:`\begin{pmatrix} u \\ v \end{pmatrix}` - pixel point coords, :math:`\begin{pmatrix} X \\ Y \\ Z \end{pmatrix}` - ray.

Projection from ray to pixel is a bit more complicated.

:math:`\begin{pmatrix} X \\ Y \\ Z \end{pmatrix}` - ray.

.. math::
  \begin{equation}
  r = \sqrt{X^2 + Y^2} \\
  \rho'(r) = a_0 - \frac{Z}{r} + a_1r + ...\\
  r_0 - smallest\: real\: positive\: root\: of\: \rho'(r) \\
  \begin{pmatrix} u' \\ v' \end{pmatrix} =\begin{pmatrix} X \frac{r_0}{r} \\ Y \frac{r_0}{r} \end{pmatrix} \\
  \begin{pmatrix} u \\ v \end{pmatrix} = Stretch \begin{pmatrix} u' \\ v' \end{pmatrix} + \begin{pmatrix} p_x \\ p_y \end{pmatrix}
  \end{equation},

where :math:`\begin{pmatrix} p_x \\ p_y \end{pmatrix}`- principal point, :math:`\begin{pmatrix} u \\ v \end{pmatrix}` - pixel coordinates.


C++ Api
-------
Include `camera_model/OmnidirectionalCamera.h` to use this model.

:class:`OmnidirectionalModel`
_____________________________


This model is implemented in :class:`OmnidirectionalModel` class. :class:`OmnidirectionalModel` could be constructed from all above parameters, stored in :class:`Eigen::Vector`. Stretch Matrix coudl be stored in :class:`Eigen::Vector3d` or in :class:`Eigen::Matrix2d`.


.. code-block:: c++

  // a b
  // c 1
  // StretchMatrix as Vector3d is (a, b, c);
  Eigen::Vector3d stretchMatrix(1, 0, 0);
  // or Eigen::Matrix2d stretchMatrix;  stretchMatrix<<a,b,c,1;

  Eigen::Vector2d principal(500, 500);
  Eigen::VectorXd coeffs(3);
  coeffs << 500.549, -0.000233596, -0.0000000125904;
  
  OmnidirectionalModel omnidir(stretchMatrix, principal, coeffs);
  omnidir.height = 120000;
  // Project 3d point to the image plane
  omnidir.project(Eigen::Vector3d(4, 5, 10));
  
  // Project image point to the ray
  omnidir.projectBack(Eigen::Vector2d(120,  250));



:class:`OmnidirectionalModel` methods
_______________________________________

.. function:: Eigen::Vector2d OmnidirectionalModel::project(const Eigen::Vector3d &point)

projects 3d ray to image plane

.. function:: Eigen::Vector3d OmnidirectionalModel::projectBack(const Eigen::Vector2d &point) 

projects pixel points to 3d rays

.. function::  Eigen::Matrix2d OmnidirectionalModel::getStretchMatrix()

.. function::  Eigen::Vector2d OmnidirectionalModel::getPrincipal()

.. function::  Eigen::VectorXd OmnidirectionalModel::getPolynom()

.. function:: void OmnidirectionalModel::saveToFile(const std::string &fname)

saves parameters to file, which could be use in `PinHoleModel::loadFromFile()` to load params to another pinhole model.

.. function:: void OmnidirectionalModel::loadFromFile(const std::strng &fname)

load model parameters from file generated in `PinHoleModel::saveToFile()`

.. function:: void OmnidirectionalModel::prnt()

couts model parameters

.. function:: void OmnidirectionalModel::exportToMatlab(const std::string &fname) 

generate script with name `fname` which fill matlab `cameraParameters` with current model parameters

.. NOTE ::

  Due to limitations of Fisheye model parameters size in MatLab, exported model could lose accuracy in terms of reprojection error. 



:class:`OmnidirectionalCalibrationParams`
_________________________________________

Antoher way to construct an object of :class:`OmnidirectionalModel` is to use :class:`OmnidirectionalCalibrationParams`. This approach is used for constructing objects for calibration.
OmnidirectionalModel also could be constructed by width, height and OmnidirectionalCalibrationParams calss instance.

.. code-block:: c++

  // Polynom has a degree of 5
  OmnidirectionalCalibrationParams params(5);
  params.identStretch = true; // Stretch Matrix would be ident after calibration
  OmnidirectionalModel omnidir(my_width, my_height, params);

OmnidirectionalCalibrationParams is used to store calibration restrictions: fixedPrincipal or fixedPolynom. 

Python
------

Python usage is almost the same as C++. 

Constructing from model parameters:

.. code-block:: python

  import super_calibration as sc
  stretch = ((1,0), (0,1)) # in the python api it is only available to use 2d matrix
  principal = (50, 120)
  poly = (1,2,3,4,5,6)
  omnidir = sc.OmnidirectionalModel(stretch, principal, poly)
  omnidir.project((1,2,4))

Constructing by :class:`OmnidirectionalCalibrationParams`:

.. code-block:: python
  
  # Polynomial degree is 6
  params = sc.OmnidirectionalCalibrationParams(6)
  params.identStretch = True 
  omnidir = sc.OmnidirectionalModel(my_width, my_height, params)


Python Api has some extra constructing methods

.. code-block:: python
  
  omnidir = sc.createCamera("Omnidirectional", my_width, my_height, 
                            polynom_n = 4, diagonal_stretch = True)




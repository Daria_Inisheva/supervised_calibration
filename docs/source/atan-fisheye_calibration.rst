
.. highlight:: c++

.. default-domain:: cpp

.. _chapter-atan-fisheye_calibration:

Atan-fisheye calibration 
=========================

Calibration of atan-fisheye camera model could be separated into 2 stages:

  1. Linear estimation of pattern poses and non-linear optimization of these poses on the pattern plane.
  2. Non-linear optimization of model parameters and poses.

Pattern Poses estimation
--------------------------

Firstly Pose of each pattern is computed using DLT method. Which could find hompgraphy by solving linear system. For system like :math:`x = H u` system matrix is looked like 

.. math::
   
   \begin{pmatrix}
      0 && x_i[2] u_i && - x_i[1] u_i \\
     - x_i[2] u_i && 0 && x_i[0] u_i \\
      x_i[1] u_i && -x_i[0] u && 0 \\
      &&...&&
   \end{pmatrix},

for all :math:`i = [0, N]` N - number of matching pairs of pixel point - pattern point.

Then trivial non-linear optimization is used and H is decomposed to rotation and translation:

.. math::
   
   \begin{equation}
   H_i = \begin{bmatrix} r_1 && r_2 && t \end{bmatrix} \\
   R_1 = \begin{bmatrix} r_1 && r_2 && r_1 \times r_2 \end{bmatrix} \\
   T_1 = t \\
   R_2 = \begin{bmatrix} -r_1 && -r_2 && r_1 \times r_2 \end{bmatrix} \\
   T_2 = -t
   \end{equation}


Then best pair :math:`[R_i, T_i]` is pair which project most pixel point to be directed with their matching pattern ray.

Non-linear optimization
------------------------

On this stage we minimize reproection error by all camera model parameters and pattern poses.

.. math::

   \arg\min_{E_i, M} \left( \sum_{i=0}^{N_p} \sum_{k=0}^{N_i} \left( p_k - g(E_i, P_k, M) \right)^2 \right),

where :math:`N_p` - number of pattern pictures, :math:`E_i` - i`s pattern pose, :math:`p_i` - pixel coordinate, :math:`P_i` - pattern coordinate, :math:`M` - camera model parametes, :math:`g` - projection function from rays to image plane.



C++ Api
-------

Include `flat_pattern_calibration/AtanFisheyeCalibration.h`

:class:`AtanFisheyeCalibration`
_______________________________

:class:`AtanFisheyeCalibration` is class, where Atan-fisheye mode calibration is implemented. 

.. function:: AtabFisheyeCalibration::AtabFisheyeCalibration(const std::shared_ptr<AtanFisheyeModel> &model, FlatPatterns &patterns)

.. code-block:: c++

  //atan - is std::shared_ptr<AtanFisheyeModel>
  // fps = FlatPatterns filled with matching pixle nd pattern points
  AtanFisheyeCalibration calib(atan, fps);
  
  //has ceres::CauchyLoss(2.0) as default parameter
  // without this line optimization would be perform without loss function
  calib.setLoss();
  
  //returns RMSE of pixel reprojection
  double err = calib.calibrate();

.. function:: double AtanFisheyeCalibration::calibrate()


:class:`CameraCalibration`
__________________________

Another opportunity for calibration is using :class:`CameraCalibration` class, which is templated class for calibrating camera modes. Usage could be found in `utils/calibrator.cpp` (example tool for camera caibration).
 Usage is the same:


.. code-block:: c++

  CameraCalibration<AtanFIsheyeModel> calib(atan, fps);

  //returns RMSE of pixel reprojection
  double err = calib.calibrate();


Python
------

Calibration in Python is performed by using `calibrateCamera` function. Python Api doesn`t have option to choose Loss function, it only could be set to :class:`ceres::CauchyLoss` or none.

.. code-block:: python

   atan = sc.createCamera("Atan-fisheye", my_width, my_height, polynom_n = 2, equal_focal = True)

   # True for using loss
   err = calibrateCamera(atan, fps, True)
   print("RMSE = ", err)







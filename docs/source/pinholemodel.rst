
.. highlight:: c++

.. default-domain:: cpp

.. _chapter-pinhole_model:


Pinhole model
=============

Pinhole model is implemented to be working with any distortion polynomial degree.
This model is described by *Calibration matrix* and 2 polynomials for *radial* and *tangential* distortion.

Сalibration matrix
------------------
Сalibration matrix K looks like 

.. math::

  K = \begin{pmatrix} f_x & \alpha & p_x \\  0 &f_y  &p_y \\  0 & 0  & 1  \end{pmatrix}, 

where :math:`(f_x, f_y)` - focal length, :math:`(p_x, p_y)` - principal point, :math:`\alpha` - skew.

Distortion
----------

**Radial distortion**
_____________________

Radial distortion polynomial :math:`R(r) = 1 + \sum_{i=0}^n k_ir^i` .

Radial distortion is performed in the next way:

.. math::
  \begin{equation}
  r_0 = \sqrt{x^2 + y^2} \\
  r = 1 + \sum_{i=1}^n k_ir_0^i \\
  \begin{pmatrix} 
  x_{distorted} \\
  y_{distorted} 
  \end{pmatrix} = r
  \begin{pmatrix} 
  x \\
  y 
  \end{pmatrix}
  \end{equation},

where :math:`(x, y)` - point in normalized image coordinates, :math:`(x_{distorted},y_{distorted})` - new normalized imaged coordinates of the point.


**Tangential distortion** 
_________________________

Tangential distortion polynomials:

.. math::
  
  \begin{equation}
  tg_x(x, y, r) = (2k_1xy + k_2(r^2 + 2x^2))(1 + k_3r^2 + ...) \\
  tg_y(x, y, r) = (k_1(r^2 + 2y^2) + 2k_2xy)(1 + k_3r^2 + ...)
  \end{equation}

Performing tangential distortion:

.. math::

  \begin{equation}
  r_0 = \sqrt{x^2 + y^2} \\
  x_{distorted} = x + tg_x(x,y,r_0) \\
  y_{distorted} = y + tg_y(x,y,r_0)
  \end{equation},

where :math:`\begin{pmatrix} x \\ y \end{pmatrix}` - point in normalized image coordinates, :math:`\begin{pmatrix} x_{distorted} \\ y_{distorted} \end{pmatrix}` - new normalized imaged coordinates of the point.

Projection
----------

Projection from ray :math:`\begin{pmatrix} X \\ Y \\ Z \end{pmatrix}` to the image plane:

.. math::

  \begin{equation}
  \begin{pmatrix} x \\ y \end{pmatrix} = (\frac{X}{Z}, \frac{Y}{Z}) \\
  r_0 = \sqrt{x^2 + y^2} \\
  r = 1 + \sum_{i=1}^n k_ir_0^i \\
  x_{distorted} = rx + tg_x(x,y,r_0) \\
  y_{distorted} = ry + tg_y(x,y,r_0) \\
  \begin{pmatrix}
  u \\
  v \\
  1
  \end{pmatrix} = K
  \begin{pmatrix}
  x_{distorted} \\
  y_{distorted} \\
  1
  \end{pmatrix}
  \end{equation},

where :math:`\begin{pmatrix} u \\ v \end{pmatrix}` - pixel coordinates of the point on image plane.

Projection from pixel point :math:`\begin{pmatrix} u \\ v \end{pmatrix}` to ray :math:`\begin{pmatrix} X \\ Y \\ 1 \end{pmatrix}` works like this: 

.. math::

  \begin{equation}
  \begin{pmatrix} x_0 \\ y_0 \\ 1 \end{pmatrix} = K^{-1}
  \begin{pmatrix} u \\ v \\ 1 \end{pmatrix} \\
  \rho(r) = 1 + \sum_{i=1}^n k_ir^i \\
  r_0 - smallest\: real\: positive\: root\: of\: \rho(r) \\
  \begin{pmatrix} x \\ y \\ 1 \end{pmatrix} = \begin{pmatrix} r_0x_0 \\ r_0y_0 \\ 1 \end{pmatrix} \\
  \begin{pmatrix} X \\ Y \\ 1 \end{pmatrix} = \arg\min_{\begin{pmatrix} x_0 \\ y_0 \\ 1 \end{pmatrix}} \left( proj(\begin{pmatrix} x_0 \\ y_0 \\ 1 \end{pmatrix}) - \begin{pmatrix} u \\ v \end{pmatrix} \right)^2
  \end{equation},

 


C++ Api
-------
Include `camera_models/PinHoleCamera.h` to use this model.

:class:`PinHoleModel`
_____________________

This model is implemented in :class:`PinHoleModel` class, inherited from :class:`CameraModel`. :class:`PinHoleModel` could be constructed from all above parameters, stored in :class:`Eigen::Vector`. 

.. function:: PinHoleModel::PinHoleModel(const V2 &focal, const V2 &principal, const Vx &radial, const Vx &tangential, double skew = 0)

.. function:: PinHoleModel::PinHoleModel(int w, int h, PinHoleCalibrationParams calibParams)


.. code-block:: c++

  Eigen::Vector2d focal(1200, 1400);
  Eigen::Vector2d principalPoint(500, 500);
  double skew = 0.0;
  
  Eigen::VectorXd radialDistortionPolynom(5);
  radialDistortionPolynom<<1, 2, 3, 4, 5;
  
  Eigen::VectorXd tangentialDistortionPolynom(2);
  tangentialDistortionPolynom<<1, 2;
  PinHoleModel pinhole(focal, principalPoint, radialDistortionPolynom, 
                       tangentialDistortionPolynom, skew);
  pinhole.width = 120000;
  // Project 3d point to the image plane
  pinhole.project(Eigen::Vector3d(4, 5, 10));
  
  // Project image point to the ray
  pinhole.projectBack(Eigen::Vector2d(120,  250));

:class:`PinHoleModel` methods:
____________________________________

.. function:: Eigen::Vector2d PinHoleModel::project(const Eigen::Vector3d &point)

projects 3d ray to image plane

.. function:: Eigen::Vector3d PinHoleModel::projectBack(const Eigen::Vector2d &point) 

projects pixel point to 3d ray

.. function:: Eigen::Vector2d PinHoleModel::getFocal()

.. function:: Eigen::Vector2d PinHoleModel::getPrincipal()

.. function:: double PinHoleModel::getSkew()

.. function:: Eigen::VectorXd PinHoleModel::getRadialPolynom()

.. function:: Eigen::VectorXd PinHoleModel::getTangentialPolynom()

.. function:: Eigen::Matrix2d PinHoleModel::getCalibMatrix()

.. function:: void PinHoleModel::saveToFile(const std::string &fname)

saves parameters to file, which could be use in `PinHoleModel::loadFromFile()` to load params to another pinhole model.

.. function:: void PinHoleModel::loadFromFile(const std::strng &fname)

load model parameters from file generated in `PinHoleModel::saveToFile()`

.. function:: void PinHoleModel::prnt()

couts model parameters

.. function:: void PinHoleModel::exportToMatlab(const std::string &fname) 

generate script with name `fname` which fill matlab `cameraParameters` with current model parameters

.. function:: void PinHoleModel::exportToOpencv(const std::string &fname)

generate YML file with current model parameters, which could be loaded in opencv

.. NOTE ::

  Due to limitations of PinHole model parameters size in MatLab and OpenCV, exported model could lose accuracy in terms of reprojection error. 


:class:`PinHoleCalibrationParams`
_________________________________

:class:`PinHoleCalibrationParams` stored PinHoleModel params, such as radial distortion degree and calibration parameters: fixedFocal, fixedPrincipal, zeroSkew. 

:class:`PinHoleModel` could be also construct from width, height and :class:`PinHoleCalibrationParams`. This is need to perform camera calibration.

.. function:: PinHoleCalibrationParams::PinHoleCalibrationParams(int radialN, int tangentialN)

.. code-block:: c++

  // Radial distortion has 8 elements in Vector, tangential - 2
  PinHoleCalibrationParams params(8, 2);
  params.zeroSkew = true; // skew param would be zero after calibration
  PinHoleModel pinhole(my_width, my_height, params);


Python
------

Python usage is almost the same as C++. 

Constructing from model parameters:

.. code-block:: python

  import super_calibration as sc
  focal = (100, 200)
  principal = (50, 120)
  skew = 0.5
  radial = (1, 2, 6, 8)
  tangential = (1,2 )

  pinhole = sc.PinholeModel(focal, principal, radial, tangential, skew)
  pinhole.projectBack((200, 466))

Constructing by :class:`PinHoleCalibrationParams`:

.. code-block:: python
  
  # Radial distortion with 4 degree and tangential with 2
  params = sc.PinHoleCalibrationParams(4, 2)
  params.equalFocal = True 
  pinhole = sc.PinholeModel(my_width, my_height, params)


Python Api has some extra constructing methods

.. code-block:: python
  
  pinhole = sc.createCamera("Pinhole", my_width, my_height, 
                            radial_n = 4, tangential_n = 2
                            equal_focal = True)

  pinhole.print() 





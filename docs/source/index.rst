
======================
Supervised calibration
======================

This Project is C++ library for calibrating multi-camera systems with different types of camera models. It is also supports Python bindings.

Supported camera models:

1. Pinhole with radial and tangential distortion model 
2. Atan-fisheye camera model
3. Omnidirectional camera model



.. toctree::
   :maxdepth: 10

   installation
   quick_start
   camera_models
   camera_calibration
   poses_calibration



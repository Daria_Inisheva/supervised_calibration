
.. highlight:: c++

.. default-domain:: cpp

.. _chapter-omnidirectional_calibration:

Omnidirectional calibration 
============================

This camera model calibration is done by folowing `A flexible technique for accurate omnidirectional camera calibration and structure from motion.` article.

Omnidirectional camera calibration is done by 

   1. Estimation part of all camera-pattern poses 
   2. Estimating of intrnsics and rest of poses
   3. Pattern plane non-linear optimization
   4. Image plane non-linear optimization

Estimation part of all camera-pattern poses 
--------------------------------------------

In this step first 2 columns of rotation matrix and rirst 2 terms of translation are estimating. This is done by solving such system for all pattern-pixel mathces in this observation:

.. math::
   
   \begin{bmatrix}
      -v_ix_i && -v_iy_i && u_ix_i && u_iy_i && -v_i && u_i
   \end{bmatrix} = 0

where :math:`\begin{pmatrix} x_i \\ y_i \\ 0 \end{pmatrix}` - pattern coordinates, :math:`\begin{pmatrix} u_i \\ v_i \end{pmatrix}` - pixel coordinates.

This step is implemented in:

.. function:: void OmnidirectionalProblem::solvePose(Pattern &p) 

Estimating of intrnsics and rest of poses
------------------------------------------

On this step we estimate :math:`t_3` part of translation for all observations and intrinsics polynom. Principal point is initiaized in the center of image. Thi step is done by solving following equations for :math:`t_3` and :math:`f(\rho)`.

.. math::

   \begin{equation}
      v_i(r_{31}x_i + r_{32}y_i + t3) - f(\rho)(r_{21}x_i + r_{22}y_i + t2) = 0 \\
      f(\rho)(r_{11}x_i + r_{12}y_i + t_1) - u_i(r_{31}x_i + r_{32}y_i + t_3) = 0
   \end{equation},

where :math:`r_{ij}` - rotation matrix elements, :math:`t_i` - translation vector elements, :math:`\begin{pmatrix} x_i \\ y_i \\ 0 \end{pmatrix}` - pattern coordinates, :math:`\begin{pmatrix} u_i \\ v_i \end{pmatrix}` - pixel coordinates.



This step is implemented in:

.. function:: void OmnidirectionalProblem::solveParameters(OmnidirectionalParams &model)


Pattern plane non-linear optimization
--------------------------------------

This step is needed to peform becuase some of extrinsics computation coud failed. On this step pattern poses and camera intrinsics are optimizing by minimizing normalized rays distance.

.. math::

   \arg\min_{E_i, M} \left( \sum_{i=0}^{N_p} \sum_{k=0}^{N_i} \left( \frac{P_k}{||P_k||} - \frac{G(E_i, p_k, M)}{||G(E_i, p_k, M)||} \right)^2 \right),

where :math:`N_p` - number of pattern pictures, :math:`E_i` - i`s pattern pose, :math:`p_i` - pixel coordinate, :math:`P_i` - pattern coordinate, :math:`M` - camera model parametes, :math:`G` - projection function from pixels to rays.



this step is implemented in:

.. function:: double OmnidirectionalProblem::patternPlaneOptimization(const std::shared_ptr<OmnidirectionalModel> &model)

Image plane non-linear optimization
------------------------------------

On this stage we minimize reproection error by all camera model parameters and pattern poses.

.. math::

   \arg\min_{E_i, M} \left( \sum_{i=0}^{N_p} \sum_{k=0}^{N_i} \left( p_k - g(E_i, P_k, M) \right)^2 \right),

where :math:`N_p` - number of pattern pictures, :math:`E_i` - i`s pattern pose, :math:`p_i` - pixel coordinate, :math:`P_i` - pattern coordinate, :math:`M` - camera model parametes, :math:`g` - projection function from rays to image plane.

this step is implemented in:

.. function:: OmnidirectionalCalibration::globalNNOptimization(ceres::LossFunction *loss_)

C++ Api
-------

Include `flat_pattern_caibration/OmnidirectionalCalibration.h`

:class:`OmnidirectionlCalibration`
____________________________________

:class:`OmnidirectionlCalibration` is class, where Omnidirectionl calibration is implemented.

.. function:: OmnidirectionalCalibration::OmnidirectionalCalibration(const std::shared_ptr<OmnidirectionalModel> &model, FlatPatterns &patterns)

Also it supports including loss function, see `ceres-solver Loss doc`_ for addition information.

.. _ceres-solver Loss doc: http://ceres-solver.org/nnls_modeling.html#lossfunction

.. function:: void OmnidirectionalCalibration::setLoss(ceres::LossFunction \*f = new ceres::CauchyLoss(2,0))


.. code-block:: c++

   //omnid - is std::shared_ptr<OmnidirectionalModel>
   // fps = FlatPatterns filled with matching pixle nd pattern points
   OmnidirectionalCalibration calib(omnid, fps);

   //has ceres::CauchyLoss(2.0) as default parameter
   // without this line optimization would be perform without loss function
   calib.setLoss();

   //returns RMSE of pixel reprojection
   double err = calib.calibrate();

.. function:: double OmnidirectionalCalibration::calibrate()

perform calibration

:class:`CameraCalibration`
__________________________

Another opportunity for calibration is using :class:`CameraCalibration` class, which is templated class for calibrating camera modes. Usage could be found in `utils/calibrator.cpp` (example tool for camera caibration).
 Usage is the same:


.. code-block:: c++

   //omnidir - is std::shared_ptr<OmnidirectionalModel>
   // fps = FlatPatterns filled with matching pixle nd pattern points
   CameraCalibration<OmnidirectionalModel> calib(omnidir, fps);

   calib.setLoss(new myLoss(42));

   //returns RMSE of pixel reprojection
   double err = calib.calibrate();

Python
------

Calibration in Python is performed by using `calibrateCamera` function. Python Api doesn`t have option to choose Loss function, it only could be set to :class:`ceres::CauchyLoss` or none.

.. code-block:: python

   omnidir = sc.createCamera("Omnidirectional", my_width, my_height, 
                            polynom_n = 4, 
                            fixed_stretch = True)

   # True for using loss
   err = calibrateCamera(omnidir, fps, True)
   print("RMSE = ", err)








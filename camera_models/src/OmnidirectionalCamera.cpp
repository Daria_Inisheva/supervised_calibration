#include "camera_models/OmnidirectionalCamera.h"
#include "camera_models/OmnidirectionalCosts.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <thread>

OmnidirectionalModel::OmnidirectionalModel(
    int w, int h, OmnidirectionalCalibrationParams calibParams_)
    : calibParams(calibParams_) {
  int degree = calibParams_.rN;
  modelParams = Eigen::VectorXd(2 + 2 + degree);
  modelParams.setZero();
  Eigen::Matrix<double, 4, 1> fpart;
  fpart << 1, 0, w / 2, h / 2;
  modelParams.template head<4>() = fpart;
  modelParams[4] = (h + w) / 2;
  width = w;
  height = h;
  calibParams.recalculate();
}

OmnidirectionalModel::OmnidirectionalModel(const Eigen::Vector2d &stretchMatrix,
                                           const Eigen::Vector2d &principal,
                                           const Eigen::VectorXd &polyCoeffs) {

  modelParams = Eigen::VectorXd(2 + 2 + polyCoeffs.rows());
  modelParams << stretchMatrix, principal, polyCoeffs;
  CHECK_GT(polyCoeffs[0], 0.);
  calibParams = OmnidirectionalCalibrationParams(getDegree());
}

void OmnidirectionalModel::saveToFile(const std::string &fname) {
  std::ofstream file(fname);
  file << getDegree() << " ";
  file << std::setprecision(16) << modelParams[0] << " " << modelParams[1]
       << " ";
  file << std::scientific << getPrincipal().transpose() << " ";
  file << width << " " << height << " ";
  auto poly = getPolynom();
  file << poly.transpose() << '\n';
}

void OmnidirectionalModel::loadFromFile(const std::string &fname) {
  std::ifstream file(fname);
  int rN;
  file >> rN;
  modelParams = Eigen::VectorXd(4 + rN);
  file >> modelParams[0] >> modelParams[1]; // Stretch
  file >> modelParams[2] >> modelParams[3]; // principal
  for (int i = 0; i < rN; ++i)
    file >> modelParams[4 + i];
  calibParams = OmnidirectionalCalibrationParams(rN);
}

OmnidirectionalModel::OmnidirectionalModel(const Eigen::Matrix2d &stretch,
                                           const Eigen::Vector2d &principal,
                                           const Eigen::VectorXd &polyCoeffs) {
  modelParams = Eigen::VectorXd(2 + 2 + polyCoeffs.rows());
  modelParams[0] = stretch(0, 0);
  modelParams[1] = stretch(0, 1);
  modelParams[2] = principal(0);
  modelParams[3] = principal(1);
  CHECK_GT(polyCoeffs[0], 0.);
  modelParams.tail(polyCoeffs.rows()) = polyCoeffs;
  calibParams = OmnidirectionalCalibrationParams(modelParams.rows());
}

Eigen::Matrix2d OmnidirectionalModel::getStretchMatrix() const {
  Eigen::Matrix2d stretchMatrix;
  stretchMatrix(0, 0) = modelParams[0];
  stretchMatrix(0, 1) = modelParams[1];
  stretchMatrix(1, 0) = 0.;
  stretchMatrix(1, 1) = 1.;
  return stretchMatrix;
}

void OmnidirectionalModel::setPrincipal(const Eigen::Vector2d &newPrincipal) {
  modelParams[2] = newPrincipal(0);
  modelParams[3] = newPrincipal(1);
}
void OmnidirectionalModel::setStretchMatrix(const Eigen::Matrix2d &newStretch) {
  modelParams(0) = newStretch(0, 0);
  modelParams(1) = newStretch(0, 1);
  CHECK_EQ(newStretch(1, 0), 0.);
  CHECK_EQ(newStretch(1, 1), 1.);
}

void OmnidirectionalModel::convertModel(double scale,
                                        const Eigen::Vector2d &center,
                                        const Eigen::VectorXd &polyCoeffs) {
  modelParams = Eigen::VectorXd(2 + 2 + polyCoeffs.rows());
  modelParams.setZero();
  Eigen::Vector2d principal = center * scale;
  Eigen::Vector2d stretchMatrix(1, 0);
  CHECK_GT(polyCoeffs[0], 0.);

  double scale_step = scale;
  Eigen::VectorXd polynom = polyCoeffs;
  polynom[0] *= scale;
  int degree = getDegree();
  for (int i = 1; i < degree; ++i) {
    polynom(i) /= scale_step;
    scale_step *= scale;
  }
  modelParams << stretchMatrix, principal, polynom;
}

Eigen::Vector2d OmnidirectionalModel::getPrincipal() {
  return Eigen::Vector2d(modelParams[2], modelParams[3]);
}

Eigen::VectorXd OmnidirectionalModel::getPolynom() const {
  Eigen::VectorXd polynom = modelParams.tail(modelParams.rows() - 4);
  return polynom;
}

void OmnidirectionalModel::print() {
  std::cout << "\n Omnidirectional model params: \n";
  std::cout << " stretchMatrix :\n" << getStretchMatrix();
  std::cout << "\n Principal : " << getPrincipal().transpose();
  std::cout << "\n Poly : " << getPolynom().transpose() << std::endl;
  ;
}

int OmnidirectionalModel::getDegree() const { return modelParams.rows() - 4; }

void OmnidirectionalModel::setCalibParams(CameraCalibrationParams *params) {
  calibParams = *(OmnidirectionalCalibrationParams *)params;
}

ceres::LocalParameterization *
OmnidirectionalModel::createLocalParametrization() {
  calibParams.recalculateSizes();
  return LocalParameterizationOmnidir::create(calibParams);
}

ceres::LocalParameterization *OmnidirectionalModel::getLocalParametrization(
    OmnidirectionalCalibrationParams *params) {
  params->recalculateSizes();
  return LocalParameterizationOmnidir::create(*params);
}

ceres::LocalParameterization *
OmnidirectionalModel::getLocalParametrization(CameraCalibrationParams *params) {
  params->recalculateSizes();
  return LocalParameterizationOmnidir::create(
      *(OmnidirectionalCalibrationParams *)params);
}

ceres::CostFunction *
OmnidirectionalModel::getGlobalRelativeCost(const Eigen::Vector2d &pixel,
                                            const Eigen::Vector3d &pattern) {

  return omnidirectional_costs::GlobalRelativeCost::create(
      pixel, pattern, modelParams.rows(), width, height);
}

ceres::CostFunction *
OmnidirectionalModel::getCameraConversionCost(const Eigen::Vector2d &pixel,
                                              const Eigen::Vector3d &pattern) {

  return omnidirectional_costs::ConversionToOmnidirFunctor::create(
      pixel, pattern, modelParams.rows());
}

double OmnidirectionalModel::convertFromModel(
    const std::shared_ptr<CameraModel> &from) {

  LOG(INFO) << "starting conversion to Omnidir model";
  width = from->width;
  height = from->height;

  modelParams = Eigen::VectorXd(4 + calibParams.rN);
  modelParams.setZero();
  LOG(INFO) << "Width = " << width;
  LOG(INFO) << "Height = " << height;
  LOG(INFO) << "Coeffs size = " << modelParams.rows();

  modelParams.head(2) = Eigen::Vector2d(1, 0);

  LOG(INFO) << "Streatch matrix =\n" << getStretchMatrix();
  Eigen::Vector2d pp = from->getPrincipal();
  modelParams(2) = pp(0);
  modelParams(3) = pp(1);
  LOG(INFO) << "Principal point = " << pp.transpose();

  if (getDegree() > 0 && !(calibParams.fixOdd && calibParams.fixEven)) {

    // Starting linear estimation of polynom
    // idea is to make z = p(r) == projected ray(2)

    LOG(INFO) << "Starting linear estimation of polynom";
    int ptsN = 0;
    for (double r = 10; r < (width - pp(0)); r += double(width - pp(0)) / 100)
      ptsN++;

    int degCount = 1;
    if (!calibParams.fixOdd)
      degCount += (getDegree() - 1) / 2;
    if (!calibParams.fixEven)
      degCount += (getDegree() - 1) / 2 + (getDegree() - 1) % 2;

    Eigen::Matrix<double, -1, -1> A(ptsN, degCount);
    Eigen::VectorXd B(ptsN);

    int inargc = 0;
    for (double r = 10; r < (width - pp(0)); r += double(width - pp(0)) / 100) {
      Eigen::Vector2d u(pp(0) + r, pp(1));
      Eigen::Vector3d ray = from->projectBack(u);
      Eigen::Vector2d x = getStretchMatrix().inverse() * (u - getPrincipal());

      Eigen::Vector2d rayH = ray.head<2>();

      double ans = rayH.dot(x) / rayH.squaredNorm();
      ray *= ans;

      double r_step = x.squaredNorm();
      double R = x.norm();

      B(inargc) = ray(2);
      A(inargc, 0) = 1;
      int arc = 1;
      for (int i = 1; i < getDegree(); ++i) {
        if (!calibParams.fixEven && i % 2 == 1)
          A(inargc, arc++) = r_step;
        if (!calibParams.fixOdd && i % 2 == 0)
          A(inargc, arc++) = r_step;
        r_step *= R;
      }
      inargc++;
    }
    Eigen::VectorXd ans = A.colPivHouseholderQr().solve(B);

    LOG(INFO) << "Linear params = " << ans.transpose();
    int arc = 1;
    modelParams(4) = ans(0);
    for (int i = 1; i < getDegree(); ++i) {
      if (!calibParams.fixEven && i % 2 == 1)
        modelParams(4 + i) = ans(arc++);
      if (!calibParams.fixOdd && i % 2 == 0)
        modelParams(4 + i) = ans(arc++);
    }
  }
  LOG(INFO) << " Estimated polynom" << getPolynom();

  calibParams.fixedPrincipal = true;
  calibParams.recalculateSizes();

  LOG(INFO) << "Starting non linear optimization";
  double error = convertOptimization(from);
  LOG(INFO) << "Error with no Loss = " << error;
  error = convertOptimization(from, new ceres::CauchyLoss(2.0));

  calibParams.fixedPrincipal = false;
  calibParams.recalculateSizes();

  LOG(INFO) << "Error with Cauchy Loss = " << error;

  return error;
}

double OmnidirectionalModel::convertOptimization(
    const std::shared_ptr<CameraModel> &from, ceres::LossFunction *loss) {
  ceres::Problem problem;
  problem.AddParameterBlock(modelParams.data(), modelParams.rows(),
                            createLocalParametrization());

  int ptN = 0;
  for (int x = 10; x < width; x += width / 40)
    for (int y = 10; y < height; y += height / 40) {
      ptN++;
      Eigen::Vector2d u(x, y);
      Eigen::Vector3d ray = from->projectBack(u);
      if (!std::isfinite(ray[0]) || !std::isfinite(ray[1]) ||
          !std::isfinite(ray[2]))
        continue;

      auto cost = getCameraConversionCost(u, ray);
      problem.AddResidualBlock(cost, loss, modelParams.data());
    }

  ceres::Solver::Options options;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = false;
  options.num_threads = std::thread::hardware_concurrency();
  options.max_num_iterations = 1000;
  options.parameter_tolerance = 1e-16;
  options.gradient_tolerance = 1e-16;
  options.function_tolerance = 1e-16;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  LOG(INFO) << summary.FullReport();
  double init_error = sqrt(2 * summary.initial_cost / ptN);
  LOG(INFO) << " Initial RMSE = " << init_error;

  double error = sqrt(2 * summary.final_cost / ptN);
  return error;
}

void OmnidirectionalModel::exportToOpencv(const std::string &fname) {
  CHECK(false) << "OPENCV is not support this model";
}
void OmnidirectionalModel::exportToMatlab(const std::string &fname) {
  Eigen::VectorXd oC(4);
  Eigen::Matrix2d s;
  Eigen::Vector2d p;
  oC.setZero();
  if (getDegree() <= 4) {
    LOG(INFO) << "Polynom size is less than 4, exporting without loss";
    LOG(INFO) << "Polynom size is " << getDegree();
    oC.head(getDegree()) = getPolynom();
    p = getPrincipal();
    s = getStretchMatrix();
  } else {
    std::cout << "\nPotential Loss of accuracy. ";
    std::cout << "Polynom size is more than 4, exporting with converting to "
                 "model of size 4\n";

    LOG(INFO) << "Potential Loss of accuracy";
    LOG(INFO) << "Polynom size is more than 4, exporting wit converting to "
                 "model with size 4";
    LOG(INFO) << "Polynom size is " << getDegree();
    OmnidirectionalCalibrationParams newPs = calibParams;
    newPs.rN = 4;
    newPs.recalculateSizes();
    OmnidirectionalModel newCam(width, height, newPs);
    std::shared_ptr<OmnidirectionalModel> cam(new OmnidirectionalModel(*this));
    newCam.convertFromModel(cam);
    p = newCam.getPrincipal();
    s = newCam.getStretchMatrix();
    oC = newCam.getPolynom();
  }

  std::ofstream outfile(fname, std::ios::out);

  outfile << "distortionCenter = [" << p(0) << " " << p(1) << "];\n";

  outfile << "imageSize = [" << height << " " << width << "];\n";

  outfile << "stretchMatrix = [" << s(0, 0) << " " << s(0, 1) << "; " << s(1, 0)
          << " " << s(1, 1) << "];\n";

  outfile << "mappingCoefficients = [" << oC(0) << " " << oC(1) << " " << oC(2)
          << " " << oC(3) << "];\n";

  outfile << "intrinsics = fisheyeIntrinsics(mappingCoefficients, imageSize, "
             "distortionCenter, stretchMatrix);\n";

  outfile.close();
}

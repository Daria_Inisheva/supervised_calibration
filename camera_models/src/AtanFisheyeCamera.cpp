#include "camera_models/AtanFisheyeCamera.h"
#include "camera_models/AtanFisheyeCosts.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <thread>

AtanFisheyeModel::AtanFisheyeModel(int w, int h,
                                   AtanCalibrationParams calibParams_) {
  calibParams = calibParams_;
  int model_degree = calibParams_.rN;
  modelParams = Eigen::VectorXd(2 + 2 + 1 + model_degree);
  modelParams.template head<2>() = V2((w + h) / 2, (w + h) / 2);
  modelParams[2] = w / 2.0;
  modelParams[3] = h / 2.0;
  modelParams[4] = 0;

  Eigen::VectorXd polyCoeffs = Eigen::VectorXd(model_degree);

  // Fill coeffs with zero distortion
  // theta + k1 theta^2 ... = r
  Eigen::Matrix<double, -1, -1> A(model_degree, model_degree);
  Eigen::Matrix<double, -1, 1> B(model_degree, 1);

  for (int i = 0; i < model_degree; ++i) {
    double val = h / 2 + i * h / (2 * model_degree);
    A(i, 0) = val * val;
    for (int k = 1; k < model_degree; ++k)
      A(i, k) = A(i, k - 1) * val;
    B(i) = atan(val);
  }
  polyCoeffs = A.householderQr().solve(B);

  modelParams.tail(model_degree) = polyCoeffs;
  width = w;
  height = h;
  calibParams.recalculate();
}

void AtanFisheyeModel::saveToFile(const std::string &fname) {
  std::ofstream file(fname);
  file << (modelParams.rows() - 5) << " ";
  file << std::setprecision(16) << std::scientific;
  file << getFocal().transpose() << " ";
  file << getPrincipal().transpose() << " ";
  file << width << " " << height << " ";
  file << getSkew() << " ";
  file << getPolynom().transpose();
}

void AtanFisheyeModel::loadFromFile(const std::string &fname) {
  std::ifstream file(fname);
  int N;
  file >> N;
  modelParams = Eigen::VectorXd(5 + N);
  file >> modelParams[0] >> modelParams[1]; // focal
  file >> modelParams[2] >> modelParams[3]; // principal
  file >> modelParams[4];                   // skew
  for (int i = 0; i < N; ++i)
    file >> modelParams[5 + i];
  calibParams = AtanCalibrationParams(N);
}

Eigen::Matrix3d AtanFisheyeModel::getCalibMatrix() const {
  Eigen::Matrix3d K = Eigen::Matrix3d::Identity();
  K(0, 0) = modelParams[0];
  K(1, 1) = modelParams[1];
  K(0, 2) = modelParams[2];
  K(1, 2) = modelParams[3];
  K(0, 1) = modelParams[4];
  return K;
}

AtanFisheyeModel::AtanFisheyeModel(const V2 &focal, const V2 &principal,
                                   const Eigen::VectorXd &polyCoeffs,
                                   double skew) {
  modelParams = Eigen::VectorXd(2 + 2 + 1 + polyCoeffs.rows());
  modelParams.template head<2>() = focal;
  modelParams[2] = principal(0);
  modelParams[3] = principal(1);
  modelParams[4] = skew;
  modelParams.tail(polyCoeffs.rows()) = polyCoeffs;
  calibParams = AtanCalibrationParams(modelParams.rows() - 5);
}

Eigen::Vector2d AtanFisheyeModel::getFocal() const {
  return modelParams.template head<2>().eval();
}

Eigen::Vector2d AtanFisheyeModel::getPrincipal() {
  Eigen::Vector2d principal(modelParams[2], modelParams[3]);
  return principal;
}

double AtanFisheyeModel::getSkew() const { return modelParams[4]; }
Eigen::VectorXd AtanFisheyeModel::getPolynom() const {
  return modelParams.tail(modelParams.rows() - 5).eval();
}

void AtanFisheyeModel::setSkew(const double &newSkew) {
  modelParams[4] = newSkew;
}

void AtanFisheyeModel::setFocal(const Eigen::Vector2d &newFocal) {
  modelParams.head(2) = newFocal;
}
void AtanFisheyeModel::setPrincipal(const Eigen::Vector2d &newPrincipal) {
  modelParams[2] = newPrincipal(0);
  modelParams[3] = newPrincipal(1);
}

void AtanFisheyeModel::print() {
  std::cout << "AtanFisheye model params: \n";
  std::cout << "Focal : " << getFocal().transpose() << std::endl;
  std::cout << "Principal : " << getPrincipal().transpose() << std::endl;
  std::cout << "Skew : " << getSkew() << std::endl;
  std::cout << "Polynom : " << getPolynom().transpose() << std::endl;
}

void AtanFisheyeModel::setCalibParams(CameraCalibrationParams *params) {
  calibParams = *(AtanCalibrationParams *)params;
}

ceres::LocalParameterization *AtanFisheyeModel::createLocalParametrization() {
  calibParams.recalculateSizes();
  return LocalParameterizationAtan::create(calibParams);
}

ceres::LocalParameterization *
AtanFisheyeModel::getLocalParametrization(AtanCalibrationParams *params) {
  params->recalculateSizes();
  return LocalParameterizationAtan::create(*params);
}

ceres::LocalParameterization *
AtanFisheyeModel::getLocalParametrization(CameraCalibrationParams *params) {
  params->recalculateSizes();
  return LocalParameterizationAtan::create(*(AtanCalibrationParams *)params);
}

ceres::CostFunction *
AtanFisheyeModel::getGlobalRelativeCost(const Eigen::Vector2d &pixel,
                                        const Eigen::Vector3d &pattern) {

  return atanfisheye_costs::GlobalRelativeCost::create(
      pixel, pattern, modelParams.rows(), width, height);
}

ceres::CostFunction *
AtanFisheyeModel::getCameraConversionCost(const Eigen::Vector2d &pixel,
                                          const Eigen::Vector3d &pattern) {

  return atanfisheye_costs::ConversionToAtanFunctor::create(pixel, pattern,
                                                            modelParams.rows());
}

double
AtanFisheyeModel::convertFromModel(const std::shared_ptr<CameraModel> &from) {
  LOG(INFO) << "starting conversion to Atan model";
  width = from->width;
  height = from->height;
  modelParams = Eigen::VectorXd(5 + calibParams.rN);
  LOG(INFO) << "Width = " << width;
  LOG(INFO) << "Height = " << height;
  LOG(INFO) << "Coeffs size = " << modelParams.rows();
  modelParams.setZero();
  Eigen::Vector2d pp = from->getPrincipal();
  modelParams(2) = pp(0);
  modelParams(3) = pp(1);
  LOG(INFO) << "Principal point = " << pp.transpose();
  // findig focal length
  // near prinicpal point distortion
  // expected to be small
  //
  {
    double f = 0.;
    for (double i = -1; i <= 1; i++)
      for (double j = -1; j <= 1; j++) {
        if (i == 0 && j == 0)
          continue;
        Eigen::Vector2d dx(i, j);
        dx.normalize();
        Eigen::Vector3d ray = from->projectBack(pp + dx);
        ray.normalize();
        double phi = std::atan2(ray.head<2>().norm(), ray.z());
        f += 0.125 / phi;
      }

    modelParams(1) = modelParams(0) = f;
  }

  LOG(INFO) << "Focal = " << getFocal().transpose();
  LOG(INFO) << "Skew = " << getSkew();

  // starting linear estimation of polynom
  if (modelParams.rows() > 5 && !(calibParams.fixEven && calibParams.fixOdd)) {
    LOG(INFO) << "Starting linear estimation of polynom";
    int paramsCount = modelParams.rows() - 5;
    int coeffsN = 0;
    if (!calibParams.fixEven)
      coeffsN += paramsCount / 2;
    if (!calibParams.fixOdd)
      coeffsN += paramsCount / 2 + paramsCount % 2;

    int ptsN = 0;
    for (double r = 10; r < (width - pp(0)); r += double(width - pp(0)) / 100)
      ptsN++;

    Eigen::Matrix<double, -1, -1> A(2 * ptsN, coeffsN);
    Eigen::VectorXd B(2 * ptsN);
    int inargcount = 0;

    for (double r = 10; r < (width - pp(0)); r += double(width - pp(0)) / 100) {
      Eigen::Vector2d u(pp(0) + r, pp(1));
      Eigen::Vector3d ray = from->projectBack(u);

      Eigen::Vector2d x = u;
      x(0) = (u(0) - getPrincipal()(0)) / getFocal()(0);
      x(1) = (u(1) - getPrincipal()(1)) / getFocal()(1);

      double R = ray.head(2).norm();
      double theta = atan2(R, ray(2));
      ray /= R;
      ray *= theta;
      B(inargcount) = x(0) - ray(0);
      B(inargcount + 1) = x(1) - ray(1);

      double theta_step = theta;
      int paramC = 0;
      for (int i = 0; i < modelParams.rows() - 5; ++i) {
        if (!calibParams.fixEven && i % 2 == 1) {
          A(inargcount, paramC) = theta_step * ray(0);
          A(inargcount + 1, paramC++) = theta_step * ray(1);
        }
        if (!calibParams.fixOdd && i % 2 == 0) {
          A(inargcount, paramC) = theta_step * ray(0);
          A(inargcount + 1, paramC++) = theta_step * ray(1);
        }
        theta_step *= theta;
      }
      inargcount += 2;
    }
    Eigen::VectorXd ans = A.colPivHouseholderQr().solve(B);
    LOG(INFO) << "Estimated coeffs = " << ans.transpose();
    int paramC = 0;
    for (int i = 0; i < paramsCount; ++i) {
      modelParams(5 + i) = 0;
      if (!calibParams.fixEven && i % 2 == 1)
        modelParams(5 + i) = ans(paramC++);
      if (!calibParams.fixOdd && i % 2 == 0)
        modelParams(5 + i) = ans(paramC++);
    }
  }
  calibParams.fixedPrincipal = true;
  calibParams.recalculateSizes();

  LOG(INFO) << "Starting non linear optimization";
  double error = convertOptimization(from);
  LOG(INFO) << "Error with no Loss = " << error;
  error = convertOptimization(from, new ceres::CauchyLoss(2.0));

  calibParams.fixedPrincipal = false;
  calibParams.recalculateSizes();

  LOG(INFO) << "Error with Cauchy Loss = " << error;
  return error;
}

double
AtanFisheyeModel::convertOptimization(const std::shared_ptr<CameraModel> &from,
                                      ceres::LossFunction *loss) {

  ceres::Problem problem;
  problem.AddParameterBlock(modelParams.data(), modelParams.rows(),
                            createLocalParametrization());

  int ptN = 0;
  for (int x = 10; x < width; x += width / 40)
    for (int y = 10; y < height; y += height / 40) {
      ptN++;
      Eigen::Vector2d u(x, y);
      Eigen::Vector3d ray = from->projectBack(u);
      if (!std::isfinite(ray[0]) || !std::isfinite(ray[1]) ||
          !std::isfinite(ray[2]))
        continue;

      auto cost = getCameraConversionCost(u, ray);
      problem.AddResidualBlock(cost, loss, modelParams.data());
    }

  ceres::Solver::Options options;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = false;
  options.num_threads = std::thread::hardware_concurrency();
  options.max_num_iterations = 1000;
  options.parameter_tolerance = 1e-16;
  options.gradient_tolerance = 1e-16;
  options.function_tolerance = 1e-16;
  ceres::Solver::Summary summary;
  ceres::Solve(options, &problem, &summary);
  LOG(INFO) << summary.FullReport();
  double init_error = sqrt(2 * summary.initial_cost / ptN);
  LOG(INFO) << " Initial RMSE = " << init_error;

  double error = sqrt(2 * summary.final_cost / ptN);

  return error;
}
void AtanFisheyeModel::exportToMatlab(const std::string &fname) {
  LOG(ERROR) << "MatLab is not support this model";
  std::cout << "MatLab is not support this model\n";
}
void AtanFisheyeModel::exportToOpencv(const std::string &fname) {
  Eigen::VectorXd rad(8);
  rad.setZero();
  Eigen::Matrix3d K;
  K.setZero();

  if (calibParams.fixOdd && calibParams.rN <= 8) {
    std::cout << "Exporting without loss of accuracy. ";
    LOG(INFO) << "Exporting without loss of accuracy. ";
    K = getCalibMatrix();
    rad.head(calibParams.rN) = getPolynom();
  } else {
    std::cout << "Potential Loss of accuracy. ";
    LOG(INFO) << "Potential Loss of accuracy";

    AtanCalibrationParams newPs = calibParams;
    newPs.rN = 8;
    newPs.fixOdd = true;
    newPs.recalculateSizes();
    AtanFisheyeModel newCam(width, height, newPs);
    std::shared_ptr<AtanFisheyeModel> cam(new AtanFisheyeModel(*this));
    newCam.convertFromModel(cam);

    rad = newCam.getPolynom();
    K = newCam.getCalibMatrix();
  }
  std::ofstream outfile(fname, std::ios::out);
  outfile << "%YAML:1.0\n";

  outfile << "width: " << width << "\n";
  outfile << "height: " << height << "\n";

  outfile << "cameraMatrix: !!opencv-matrix";
  outfile << "\n   rows: 3\n   cols: 3\n   dt: d\n   data: [ ";
  outfile << K(0, 0) << ", " << K(0, 1) << ", " << K(0, 2) << ", ";
  outfile << K(1, 0) << ", " << K(1, 1) << ", " << K(1, 2) << ", ";
  outfile << K(2, 0) << ", " << K(2, 1) << ", " << K(2, 2) << " ]\n";

  outfile << "distCoeffs: !!opencv-matrix\n";
  outfile << "\n   rows: 4\n   cols: 1\n   dt: d\n   data: [ ";
  outfile << rad(1) << ", " << rad(3) << ", " << rad(5);
  outfile << ", " << tan(7) << " ]\n";

  outfile.close();
}

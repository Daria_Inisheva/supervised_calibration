#ifndef ATANFISHEYECOSTS_H
#define ATANFISHEYECOSTS_H
#include "camera_models/AtanFisheyeCamera.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>
#include <sophus/se3.hpp>

namespace atanfisheye_costs {

// In this Functor we are trying ro minimize error on the
// image plane, by projecting pattern PT with known homgoraphy
// to image
struct AtanCalibrationFunctor {
  AtanCalibrationFunctor(const Eigen::Vector2d &imagePoint,
                         const Eigen::Vector3d &patternPoint,
                         const int &modelSize)
      : modelSize(modelSize), imagePoint(imagePoint),
        patternPoint(patternPoint) {}

  template <typename T>
  bool operator()(T const *const *parameters, T *residuals) const {
    return this->operator()(parameters[0], parameters[1], residuals);
  }

  template <typename T>
  bool operator()(const T *modelParams_, const T *eucledian,
                  T *residual) const {

    typedef Eigen::Matrix<T, 3, 1> Vector3;
    typedef Eigen::Matrix<T, 2, 1> Vector2;
    typedef Eigen::Matrix<T, -1, 1> VectorX;
    Eigen::Map<const VectorX> _modelParams(modelParams_, modelSize);
    VectorX modelParams = _modelParams;
    Eigen::Map<const Sophus::SE3<T>> Trans(eucledian);
    Vector3 ray = Trans * patternPoint.cast<T>();

    Eigen::Map<Vector2> res(residual);
    res = imagePoint.cast<T>() - AtanFisheyeModel::project(ray, modelParams);

    return true;
  }

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  int modelSize;
  Eigen::Vector2d imagePoint;
  Eigen::Vector3d patternPoint;

  static ceres::CostFunction *create(const Eigen::Vector2d &imagePoint,
                                     const Eigen::Vector3d &patternPoint,
                                     const int &modelSize) {

    auto cost = new ceres::DynamicAutoDiffCostFunction<AtanCalibrationFunctor>(
        new AtanCalibrationFunctor(imagePoint, patternPoint, modelSize));

    cost->AddParameterBlock(modelSize);
    // for se3 optimization
    cost->AddParameterBlock(7);
    cost->SetNumResiduals(2);
    return cost;
  }
};

// This functor if for calibration
// it optimize rays distance
struct PatternPlaneFunctor {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  PatternPlaneFunctor(const Eigen::Vector2d &pixel,
                      const Eigen::Vector3d &pattern, const int &nParams)
      : nParams(nParams), pixel(pixel), pattern(pattern) {}
  template <typename T> bool operator()(T const *const *p, T *res) const {
    return this->operator()(p[0], p[1], res);
  }
  template <typename T>
  bool operator()(const T *params_, const T *patternTocam_,
                  T *residuals) const {
    typedef const Eigen::Matrix<T, -1, 1> VecX;
    typedef Eigen::Matrix<T, 3, 1> Vec3;
    typedef Eigen::Matrix<T, 2, 1> Vec2;
    typedef const Sophus::SE3<T> SE3T;
    Eigen::Map<VecX> params(params_, nParams);
    VecX p = params;
    Eigen::Map<SE3T> patternToCam(patternTocam_);
    Vec3 pRay = patternToCam * pattern.cast<T>();
    Vec2 pt = pixel.cast<T>();
    Eigen::Map<Vec3> res(residuals);
    res = pRay.normalized() - AtanFisheyeModel::projectBack(pt, p).normalized();
    return true;
  }

  int nParams;
  Eigen::Vector2d pixel;
  Eigen::Vector3d pattern;
  static auto create(const Eigen::Vector2d &pixel,
                     const Eigen::Vector3d &pattern, const int &nParams) {
    auto cost = new ceres::DynamicAutoDiffCostFunction<PatternPlaneFunctor>(
        new PatternPlaneFunctor(pixel, pattern, nParams));
    cost->AddParameterBlock(nParams);
    cost->AddParameterBlock(7);
    cost->SetNumResiduals(3);
    return cost;
  }
};

// This functor is for global relative optimization
struct GlobalRelativeCost {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  GlobalRelativeCost(const Eigen::Vector2d &pixel,
                     const Eigen::Vector3d &pattern, const int &nParams,
                     const int &W, const int &H)
      : pattern(pattern), pixel(pixel), nParams(nParams), H(H), W(W) {}

  template <typename T> bool operator()(T const *const *p, T *res) const {
    return this->operator()(p[0], p[1], p[2], p[3], res);
  }

  template <typename T>
  bool operator()(const T *params_, const T *patternToWorld_,
                  const T *worldToTime_, const T *systemToCam_,
                  T *residuals) const {
    typedef Eigen::Matrix<T, -1, 1> VecX;
    typedef Eigen::Matrix<T, 3, 1> Vec3;
    typedef Eigen::Matrix<T, 2, 1> Vec2;
    typedef Sophus::SE3<T> SE3T;
    Eigen::Map<const VecX> params(params_, nParams);
    VecX p = params;
    Eigen::Map<const SE3T> patternToWorld(patternToWorld_);
    Eigen::Map<const SE3T> worldToTime(worldToTime_);
    Eigen::Map<const SE3T> systemToCam(systemToCam_);

    Vec3 ray = systemToCam * worldToTime * patternToWorld * pattern.cast<T>();

    Eigen::Map<Vec2> res(residuals);
    Vec2 pix = AtanFisheyeModel::project(ray, p);
    // if (!(pix(0) >= T(0) && pix(0) < T(W)))
    //  return false;
    // if (!(pix(1) >= T(0) && pix(1) < T(H)))
    //  return false;

    res = pixel.cast<T>() - pix;
    return true;
  }

  Eigen::Vector3d pattern;
  Eigen::Vector2d pixel;
  int nParams;
  int H, W;

  static ceres::CostFunction *create(const Eigen::Vector2d &pixel,
                                     const Eigen::Vector3d &pattern,
                                     const int &nParams, const int &W,
                                     const int &H) {
    auto cost = new ceres::DynamicAutoDiffCostFunction<GlobalRelativeCost>(
        new GlobalRelativeCost(pixel, pattern, nParams, W, H));
    cost->AddParameterBlock(nParams);
    cost->AddParameterBlock(7);
    cost->AddParameterBlock(7);
    cost->AddParameterBlock(7);
    cost->SetNumResiduals(2);
    return cost;
  }
};

struct ConversionToAtanFunctor {
  ConversionToAtanFunctor(const Eigen::Vector2d &pixel,
                          const Eigen::Vector3d &pattern, const int &N)
      : pixel(pixel), pattern(pattern), N(N) {}

  template <typename T>
  bool operator()(T const *const *parameters, T *residuals) const {
    return this->operator()(parameters[0], residuals);
  }

  template <typename T> bool operator()(const T *params_, T *residuals) const {
    typedef Eigen::Matrix<T, -1, 1> VectorX;
    typedef Eigen::Matrix<T, 2, 1> Vector2;
    typedef Eigen::Matrix<T, 3, 1> Vector3;
    Eigen::Map<const VectorX> p_(params_, N);
    Eigen::Map<Vector2> res(residuals);
    VectorX p = p_;
    Vector3 ray = pattern.cast<T>();
    res = pixel.cast<T>() - AtanFisheyeModel::project(ray, p);

    return true;
  }

  Eigen::Vector2d pixel;
  Eigen::Vector3d pattern;
  int N;
  static ceres::CostFunction *create(const Eigen::Vector2d &pixel,
                                     const Eigen::Vector3d &pattern,
                                     const int &N) {

    auto cost = new ceres::DynamicAutoDiffCostFunction<ConversionToAtanFunctor>(
        new ConversionToAtanFunctor(pixel, pattern, N));

    cost->AddParameterBlock(N); // camParams
    cost->SetNumResiduals(2);
    return cost;
  }
};

} // namespace atanfisheye_costs

#endif // ATANFISHEYECOSTS_H

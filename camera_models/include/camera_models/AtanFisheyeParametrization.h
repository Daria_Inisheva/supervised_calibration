#ifndef ATANFISHEYEPARAMETRIZATION_H
#define ATANFISHEYEPARAMETRIZATION_H
#include "CameraModel.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>

struct AtanCalibrationParams : public CameraCalibrationParams {
  bool fixedFocal = false;
  bool equalFocal = false;
  bool fixedPrincipal = false;
  bool equalPrincipal = false;
  bool fixedSkew = false;
  bool zeroSkew = false;
  bool fixedPolynom = false;
  bool fixEven = false;
  bool fixOdd = false;
  int rN = 5;
  int lS = 10, gS = 10;
  bool isfixed() {
    bool fixPoly = fixedPolynom || (fixEven && fixOdd);
    bool fixSkew = fixedSkew || zeroSkew;
    return fixedFocal && fixedPrincipal && fixSkew && fixPoly;
  }
  AtanCalibrationParams() : rN(5) { recalculate(); }
  AtanCalibrationParams(const int &rN) : rN(rN) { recalculate(); }

  template <typename C>
  void addParameters(C &container, const std::string &prefix) {
    AddParameter(container, prefix, "FixFocalLength", fixedFocal,
                 "Set focal length constant");
    AddParameter(container, prefix, "SetFocalLengthsEqual", equalFocal,
                 "Set focal lengths equal");
    AddParameter(container, prefix, "FixPrincipalPoint", fixedPrincipal,
                 "Set principal point constant");
    AddParameter(container, prefix, "SetPrincipalPointEqual", equalPrincipal,
                 "Set principal point components equal");
    AddParameter(container, prefix, "FixSkew", fixedSkew, "Set skew constant");
    AddParameter(container, prefix, "ZeroSkew", zeroSkew, "Force zero-skew");
    AddParameter(container, prefix, "FixPolynomial", fixedPolynom,
                 "Set polynomial distortion constant");
    AddParameter(container, prefix, "FixEvenPolynomial", fixEven,
                 "Set even degrees of polynomial distortion constant");
    AddParameter(container, prefix, "FixOddPolynomial", fixOdd,
                 "Set odd degrees of polynomial distortion constant");
    AddParameter(container, prefix, "PolynomialDegree", rN,
                 "Polynomial degree for distortion model");
  }

  void recalculate() { recalculateSizes(); }
  void recalculateSizes() {
    lS = 0;
    gS = 5 + rN;

    if (!fixedFocal) {
      lS++;
      if (!equalFocal)
        lS++;
    }
    if (!fixedPrincipal) {
      lS++;
      if (!equalPrincipal)
        lS++;
    }
    if (!fixedSkew && !zeroSkew)
      lS++;

    if (!fixedPolynom && (!fixEven || !fixOdd)) {
      if (!fixEven)
        lS += rN / 2;
      if (!fixOdd)
        lS += rN / 2 + rN % 2;
    }
  }
};

struct LocalParameterizationAtan : public ceres::LocalParameterization {
public:
  virtual ~LocalParameterizationAtan() {}
  virtual int GlobalSize() const { return params.gS; }
  virtual int LocalSize() const { return params.lS; }
  AtanCalibrationParams params;
  LocalParameterizationAtan(const AtanCalibrationParams &_p) : params(_p) {}
  auto static create(const AtanCalibrationParams &_p) {
    auto parametrization = new LocalParameterizationAtan(_p);
    return parametrization;
  }

  virtual bool Plus(double const *x, double const *delta,
                    double *x_plus) const {
    int nargin = 0;
    for (int i = 0; i < GlobalSize(); ++i)
      x_plus[i] = x[i];

    if (!params.fixedFocal) {
      x_plus[0] += delta[nargin];
      if (params.equalFocal)
        x_plus[1] += delta[nargin];
      else
        x_plus[1] += delta[++nargin];
      nargin++;
    }

    if (!params.fixedPrincipal) {
      x_plus[2] += delta[nargin];
      if (params.equalPrincipal)
        x_plus[3] += delta[nargin];
      else
        x_plus[3] += delta[++nargin];
      nargin++;
    }

    if (!params.fixedSkew && !params.zeroSkew) {
      x_plus[4] += delta[nargin];
      nargin++;
    }
    if (params.rN != 0) {
      if (!params.fixedPolynom && (!params.fixEven || !params.fixOdd)) {
        for (int i = 0; i < params.rN; ++i) {
          if (!params.fixEven && (i + 1) % 2 == 0)
            x_plus[5 + i] += delta[nargin++];
          if (!params.fixOdd && (i + 1) % 2 == 1)
            x_plus[5 + i] += delta[nargin++];
        }
      }
    }
    return true;
  }

  virtual bool ComputeJacobian(const double *, double *j) const {

    typedef Eigen::Matrix<double, -1, -1, Eigen::RowMajor> jt;
    Eigen::Map<jt> J(j, GlobalSize(), LocalSize());
    J.setZero();
    int nargin = 0;
    if (!params.fixedFocal) {
      J(0, nargin) = 1.0;
      if (params.equalFocal)
        J(1, nargin) = 1.0;
      else
        J(1, ++nargin) = 1.0;
      nargin++;
    }

    if (!params.fixedPrincipal) {
      J(2, nargin) = 1.0;
      if (params.equalPrincipal)
        J(3, nargin) = 1.0;
      else
        J(3, ++nargin) = 1.0;
      nargin++;
    }

    if (!params.fixedSkew && !params.zeroSkew) {
      J(4, nargin) = 1.0;
      nargin++;
    }
    if (params.rN != 0) {
      if (!params.fixedPolynom && (!params.fixOdd || !params.fixEven)) {
        for (int i = 0; i < params.rN; ++i) {
          if (!params.fixEven && (1 + i) % 2 == 0)
            J(5 + i, nargin++) = 1.0;
          if (!params.fixOdd && (1 + i) % 2 == 1)
            J(5 + i, nargin++) = 1.0;
        }
      }
    }
    return true;
  }
};

#endif // ATANFISHEYEPARAMETRIZATION_H

#ifndef PINHOLECOSTS_H
#define PINHOLECOSTS_H
#include "camera_models/PinHoleCamera.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>
#include <sophus/se3.hpp>

namespace pinhole_costs {

// This functor is for global relative optimization
struct GlobalRelativeCost {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  GlobalRelativeCost(const Eigen::Vector2d &pixel,
                     const Eigen::Vector3d &pattern, const int &rN,
                     const int &tN, const int &W, const int &H)
      : pattern(pattern), pixel(pixel), rN(rN), tN(tN), W(W), H(H) {}

  template <typename T> bool operator()(T const *const *p, T *res) const {
    return this->operator()(p[0], p[1], p[2], p[3], res);
  }

  template <typename T>
  bool operator()(const T *params_, const T *patternToWorld_,
                  const T *worldToTime_, const T *systemToCam_,
                  T *residuals) const {
    typedef Eigen::Matrix<T, -1, 1> VecX;
    typedef Eigen::Matrix<T, 3, 1> Vec3;
    typedef Eigen::Matrix<T, 2, 1> Vec2;
    typedef Sophus::SE3<T> SE3T;
    Eigen::Map<const VecX> params(params_, 5 + rN + tN);
    VecX p = params;
    Eigen::Map<const SE3T> patternToWorld(patternToWorld_);
    Eigen::Map<const SE3T> worldToTime(worldToTime_);
    Eigen::Map<const SE3T> systemToCam(systemToCam_);

    Vec3 ray = systemToCam * worldToTime * patternToWorld * pattern.cast<T>();

    Eigen::Map<Vec2> res(residuals);
    Vec2 pix = PinHoleModel::project(ray, p, rN, tN);
    if (!(pix(0) >= T(0) && pix(0) < T(W)))
      return false;
    if (!(pix(1) >= T(0) && pix(1) < T(H)))
      return false;

    res = pixel.cast<T>() - pix;
    return true;
  }

  Eigen::Vector3d pattern;
  Eigen::Vector2d pixel;
  int rN, tN;
  int W, H;
  static ceres::CostFunction *create(const Eigen::Vector2d &pixel,
                                     const Eigen::Vector3d &pattern,
                                     const int &rN, const int &tN, const int &W,
                                     const int &H) {
    auto cost = new ceres::DynamicAutoDiffCostFunction<GlobalRelativeCost>(
        new GlobalRelativeCost(pixel, pattern, rN, tN, W, H));
    cost->AddParameterBlock(5 + rN + tN);
    cost->AddParameterBlock(7);
    cost->AddParameterBlock(7);
    cost->AddParameterBlock(7);
    cost->SetNumResiduals(2);
    return cost;
  }
};

// In this Functor we are trying ro minimize error on the
// image plane, by projecting pattern PT with known homgoraphy
// to image
struct PinHoleCalibrationFunctor {
  PinHoleCalibrationFunctor(const Eigen::Vector2d &imagePoint,
                            const Eigen::Vector3d &patternPoint,
                            const int &radialN, const int &tangentialN)
      : radialN(radialN), tangentialN(tangentialN), imagePoint(imagePoint),
        patternPoint(patternPoint) {}

  template <typename T>
  bool operator()(T const *const *parameters, T *residuals) const {
    return this->operator()(parameters[0], parameters[1], residuals);
  }

  template <typename T>
  bool operator()(const T *modelParams_, const T *eucledian,
                  T *residual) const {

    typedef Eigen::Matrix<T, 3, 1> Vector3;
    typedef Eigen::Matrix<T, -1, 1> VectorX;
    Eigen::Map<const VectorX> _modelParams(modelParams_,
                                           2 + 2 + 1 + radialN + tangentialN);
    VectorX modelParams = _modelParams;

    Eigen::Map<const Sophus::SE3<T>> Trans(eucledian);
    Vector3 ray = Trans * patternPoint.cast<T>();

    Eigen::Map<Eigen::Matrix<T, 2, 1>> res(residual);
    res = imagePoint.cast<T>() -
          PinHoleModel::project(ray, modelParams, radialN, tangentialN);

    return true;
  }

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  int radialN, tangentialN;
  Eigen::Vector2d imagePoint;
  Eigen::Vector3d patternPoint;

  static ceres::CostFunction *create(const Eigen::Vector2d &imagePoint,
                                     const Eigen::Vector3d &patternPoint,
                                     const int &radialN,
                                     const int &tangentialN) {

    auto cost =
        new ceres::DynamicAutoDiffCostFunction<PinHoleCalibrationFunctor>(
            new PinHoleCalibrationFunctor(imagePoint, patternPoint, radialN,
                                          tangentialN));

    cost->AddParameterBlock(2 + 2 + 1 + radialN + tangentialN); // camParams
    // for se3 optimization
    cost->AddParameterBlock(7);
    cost->SetNumResiduals(2);
    return cost;
  }
};

struct ConversionToPinHoleFunctor {
  ConversionToPinHoleFunctor(const Eigen::Vector2d &pixel,
                             const Eigen::Vector3d &pattern, const int &rN,
                             const int &tN)
      : pixel(pixel), pattern(pattern), rN(rN), tN(tN) {}

  template <typename T>
  bool operator()(T const *const *params_, T *residuals) const {
    typedef Eigen::Matrix<T, -1, 1> VectorX;
    typedef Eigen::Matrix<T, 2, 1> Vector2;
    typedef Eigen::Matrix<T, 3, 1> Vector3;
    Eigen::Map<const VectorX> p_(params_[0], 5 + rN + tN);
    Eigen::Map<Vector2> res(residuals);
    VectorX p = p_;
    Vector3 ray = pattern.cast<T>();
    res = pixel.cast<T>() - PinHoleModel::project(ray, p, rN, tN);

    return true;
  }

  Eigen::Vector2d pixel;
  Eigen::Vector3d pattern;
  int rN, tN;
  static ceres::CostFunction *create(const Eigen::Vector2d &pixel,
                                     const Eigen::Vector3d &pattern,
                                     const int &rN, const int &tN) {

    auto cost =
        new ceres::DynamicAutoDiffCostFunction<ConversionToPinHoleFunctor>(
            new ConversionToPinHoleFunctor(pixel, pattern, rN, tN));

    cost->AddParameterBlock(2 + 2 + 1 + rN + tN); // camParams
    cost->SetNumResiduals(2);
    return cost;
  }
};

} // namespace pinhole_costs

#endif // PINHOLECOSTS_H

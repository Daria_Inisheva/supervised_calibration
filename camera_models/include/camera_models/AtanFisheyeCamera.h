#ifndef __ATANFISHEYECAMERA_H
#define __ATANFISHEYECAMERA_H
#include "camera_models/AtanFisheyeParametrization.h"
#include "camera_models/CameraModel.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>

using V2 = Eigen::Vector2d;
using V3 = Eigen::Vector3d;

class AtanFisheyeModel : public CameraModel {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  AtanFisheyeModel(int w, int h, AtanCalibrationParams calibParams_);

  AtanFisheyeModel(const V2 &focal, const V2 &principal,
                   const Eigen::VectorXd &polyCoeffs, double skew = 0);
  AtanCalibrationParams calibParams;

  std::string getType() override { return "Atan-fisheye"; }
  Eigen::Vector2d getFocal() const;
  Eigen::Vector2d getPrincipal() override;
  Eigen::Matrix3d getCalibMatrix() const;
  void setSkew(const double &newSkew);
  void setFocal(const Eigen::Vector2d &newFocal);
  void setPrincipal(const Eigen::Vector2d &newPrincipal);
  void exportToMatlab(const std::string &fname) override;
  void exportToOpencv(const std::string &fname) override;
  double getSkew() const;
  Eigen::VectorXd getPolynom() const;
  void print() override;
  void loadFromFile(const std::string &fname) override;
  void saveToFile(const std::string &fname) override;

  Eigen::Vector2d project(const Eigen::Vector3d &point) override {
    return project(point, getFocal(), getPrincipal(), getSkew(), getPolynom());
  }

  Eigen::Vector3d projectBack(const Eigen::Vector2d &point) override {
    return projectBack(point, getFocal(), getPrincipal(), getSkew(),
                       getPolynom());
  }

  template <typename T>
  static Eigen::Matrix<T, 2, 1>
  project(const Eigen::Matrix<T, 3, 1> &point,
          const Eigen::Matrix<T, -1, 1> &modelParams) {
    Eigen::Matrix<T, 2, 1> focal, principal(modelParams[2], modelParams[3]);
    T skew = modelParams[4];
    Eigen::Matrix<T, -1, 1> polyCoeffs(modelParams.rows() - 5);
    focal = modelParams.template head<2>();
    polyCoeffs = modelParams.tail(modelParams.rows() - 5);
    return project(point, focal, principal, skew, polyCoeffs);
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1>
  projectBack(const Eigen::Matrix<T, 2, 1> &point,
              const Eigen::Matrix<T, -1, 1> &modelParams) {
    Eigen::Matrix<T, 2, 1> focal, principal(modelParams[2], modelParams[3]);
    T skew = modelParams[4];
    Eigen::Matrix<T, -1, 1> polyCoeffs(modelParams.rows() - 5);
    focal = modelParams.template head<2>();
    polyCoeffs = modelParams.tail(modelParams.rows() - 5);
    return projectBack(point, focal, principal, skew, polyCoeffs);
  }

  template <typename T>
  static Eigen::Matrix<T, 2, 1> project(const Eigen::Matrix<T, 3, 1> &point,
                                        const Eigen::Matrix<T, 2, 1> &focal,
                                        const Eigen::Matrix<T, 2, 1> &principal,
                                        const T &skew,
                                        const Eigen::Matrix<T, -1, 1> &coeffs) {

    Eigen::Matrix<T, 3, 1> distorted = distort(point, coeffs);
    T x = focal(0) * distorted(0) + skew * distorted(1) + principal(0);
    T y = focal(1) * distorted(1) + principal(1);
    return Eigen::Matrix<T, 2, 1>(x, y);
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1> distort(const Eigen::Matrix<T, 3, 1> &point,
                                        const Eigen::Matrix<T, -1, 1> &coeffs) {

    // polynom
    // \theta_t = atan2(r, z)
    // f(\theta) = 1 + ... + c_i*theta^i+ ...
    // point = old / r * f(\theta_t)
    T r = point.template head<2>().norm();
    if (r < Eigen::NumTraits<T>::epsilon())
      return point;
    T theta = ceres::atan2(r, point(2));
    T theta_step = theta;
    T theta_new = T(1.0);

    for (int i = 0; i < coeffs.rows(); ++i) {
      theta_new += coeffs[i] * theta_step;
      theta_step *= theta;
    }
    theta_new = theta * theta_new;

    Eigen::Matrix<T, 2, 1> distorted;
    distorted = point.template head<2>() * theta_new / r;
    return distorted.homogeneous();
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1>
  projectBack(const Eigen::Matrix<T, 2, 1> &point,
              const Eigen::Matrix<T, 2, 1> &focal,
              const Eigen::Matrix<T, 2, 1> &principal, const T &skew,
              const Eigen::Matrix<T, -1, 1> &coeffs) {

    T y = (point(1) - principal(1)) / focal(1);
    T x = (point(0) - principal(0) - skew * y) / focal(0);
    Eigen::Matrix<T, 3, 1> pt(x, y, T(1));

    return undistort(pt, coeffs);
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1>
  undistort(const Eigen::Matrix<T, 3, 1> &point,
            const Eigen::Matrix<T, -1, 1> &coeffs) {

    typedef Eigen::Matrix<T, Eigen::Dynamic, 1> VectorX;
    T r = point.template head<2>().norm();
    if (r < Eigen::NumTraits<T>::epsilon())
      return point;

    // Solving polynom
    int N = coeffs.rows();
    VectorX polyCoeffs(N + 2);
    polyCoeffs[0] = -r;
    polyCoeffs[1] = T(1.0);
    for (int i = 0; i < N; ++i)
      polyCoeffs[i + 2] = coeffs[i];

    T firstPosRoot = solve_equation(polyCoeffs);

    Eigen::Matrix<T, 3, 1> undistorted;
    undistorted.template head<2>() =
        sin(firstPosRoot) * point.template head<2>() / r;
    undistorted(2) = cos(firstPosRoot);

    return undistorted.normalized();
  }

  ceres::LocalParameterization *
  getLocalParametrization(CameraCalibrationParams *params) override;

  ceres::LocalParameterization *
  getLocalParametrization(AtanCalibrationParams *params);

  void setCalibParams(CameraCalibrationParams *params) override;
  ceres::LocalParameterization *createLocalParametrization() override;

  ceres::CostFunction *
  getGlobalRelativeCost(const Eigen::Vector2d &pixel,
                        const Eigen::Vector3d &pattern) override;

  ceres::CostFunction *
  getCameraConversionCost(const Eigen::Vector2d &pixel,
                          const Eigen::Vector3d &pattern) override;

  double convertFromModel(const std::shared_ptr<CameraModel> &from) override;

private:
  double convertOptimization(const std::shared_ptr<CameraModel> &from,
                             ceres::LossFunction *loss = nullptr);
};

#endif //__ATANFISHEYECAMERA_H

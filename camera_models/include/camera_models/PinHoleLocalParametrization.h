#ifndef PINHOLELOCALPARAMETRIZATION_H
#define PINHOLELOCALPARAMETRIZATION_H
#include "camera_models/CameraModel.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>

struct PinHoleCalibrationParams : public CameraCalibrationParams {
  bool fixedFocal = false;
  bool equalFocal = false;
  bool fixedPrincipal = false;
  bool equalPrincipal = false;
  bool fixedSkew = false;
  bool zeroSkew = false;
  bool fixedRadial = false;
  bool fixedTangential = false;
  bool fixOddRadial = false;
  bool fixEvenRadial = false;
  // this mean fixing (2a1xy + a2(r^2 + 2x^2)) part
  bool fixMainTangential = false;
  bool fixOddTangential = false;
  bool fixEvenTangential = false;
  int rN = 4, tN = 2;
  int lS = 5, gS = 5;
  bool isfixed() {
    bool fixPolyR = fixedRadial || (fixEvenRadial && fixOddRadial);
    bool fixPolyT =
        fixMainTangential && (fixEvenTangential && fixOddTangential);
    bool fixPoly = fixPolyR && fixPolyT;
    bool fixSkew = fixedSkew || zeroSkew;
    return fixedFocal && fixedPrincipal && fixSkew && fixPoly;
  }

  PinHoleCalibrationParams() : rN(4), tN(2) { this->recalculate(); }
  PinHoleCalibrationParams(int rN, int tN) : rN(rN), tN(tN) {
    this->recalculate();
  }

  template <typename C>
  void addParameters(C &container, const std::string &prefix) {
    AddParameter(container, prefix, "FixFocalLength", fixedFocal,
                 "Set focal length constant");
    AddParameter(container, prefix, "SetFocalLengthsEqual", equalFocal,
                 "Set focal lengths equal");
    AddParameter(container, prefix, "FixPrincipalPoint", fixedPrincipal,
                 "Set principal point constant");
    AddParameter(container, prefix, "SetPrincipalPointEqual", equalPrincipal,
                 "Set principal point components equal");
    AddParameter(container, prefix, "FixSkew", fixedSkew, "Set skew constant");
    AddParameter(container, prefix, "ZeroSkew", zeroSkew, "Force zero-skew");

    AddParameter(container, prefix, "FixRadialPolynomial", fixedRadial,
                 "Set radial distortion constant");
    AddParameter(container, prefix, "FixEvenRadialPolynomial", fixEvenRadial,
                 "Set even degrees of radial distortion constant");
    AddParameter(container, prefix, "FixOddRadialPolynomial", fixOddRadial,
                 "Set even degrees of radial distortion constant");
    AddParameter(container, prefix, "PolynomialDegreeRadial", rN,
                 "Polynomial degree for radial distortion");
    AddParameter(container, prefix, "FixTangentialPolynomial", fixedTangential,
                 "Set tangential distortion constant");
    AddParameter(container, prefix, "FixSecondOrderTangential",
                 fixMainTangential,
                 "Set second-order part of tangential distortion constant");
    AddParameter(container, prefix, "FixEvenTangentialPolynomial",
                 fixEvenTangential,
                 "Set even degrees of tangential distortion constant");
    AddParameter(container, prefix, "FixOddTangentialPolynomial",
                 fixOddTangential,
                 "Set odd degrees of tangential distortion constant");
  }

  void recalculate() { recalculateSizes(); }
  void recalculateSizes() {
    gS = 2 + 2 + 1 + rN + tN;
    lS = 0;
    if (!fixedFocal) {
      lS++;
      if (!equalFocal)
        lS++;
    }
    if (!fixedPrincipal) {
      lS++;
      if (!equalPrincipal)
        lS++;
    }
    if (!fixedSkew && !zeroSkew)
      lS++;

    if (!fixedRadial && (!fixOddRadial || !fixEvenRadial)) {
      if (!fixEvenRadial)
        lS += rN / 2;
      if (!fixOddRadial)
        lS += rN / 2 + rN % 2;
    }

    if (!fixedTangential &&
        (!fixMainTangential || !fixOddTangential || !fixEvenTangential)) {

      if (!fixMainTangential) {
        lS += 2;
      }
      if (!fixEvenTangential)
        lS += (tN - 2) / 2 + (tN - 2) % 2;
      if (!fixOddTangential)
        lS += (tN - 2) / 2;
    }
  }
};

struct LocalParameterizationPinHole : public ceres::LocalParameterization {
public:
  virtual ~LocalParameterizationPinHole() {}
  virtual int GlobalSize() const { return params.gS; }
  virtual int LocalSize() const { return params.lS; }
  PinHoleCalibrationParams params;
  LocalParameterizationPinHole(const PinHoleCalibrationParams &_p)
      : params(_p) {}
  auto static create(const PinHoleCalibrationParams &_p) {
    auto parametrization = new LocalParameterizationPinHole(_p);
    return parametrization;
  }

  virtual bool Plus(double const *x, double const *delta,
                    double *x_plus) const {
    int nargin = 0;

    for (int i = 0; i < GlobalSize(); ++i)
      x_plus[i] = x[i];

    if (!params.fixedFocal) {
      x_plus[0] += delta[nargin];
      if (params.equalFocal)
        x_plus[1] += delta[nargin];
      else
        x_plus[1] += delta[++nargin];
      nargin++;
    }

    if (!params.fixedPrincipal) {
      x_plus[2] += delta[nargin];
      if (params.equalPrincipal)
        x_plus[3] += delta[nargin];
      else
        x_plus[3] += delta[++nargin];
      nargin++;
    }

    if (!params.fixedSkew && !params.zeroSkew) {
      x_plus[4] += delta[nargin];
      nargin++;
    }
    if (params.rN != 0) {
      if (!params.fixedRadial &&
          (!params.fixOddRadial || !params.fixEvenRadial)) {
        for (int i = 0; i < params.rN; ++i) {
          if (!params.fixEvenRadial && (1 + i) % 2 == 0)
            x_plus[5 + i] += delta[nargin++];
          if (!params.fixOddRadial && (1 + i) % 2 == 1)
            x_plus[5 + i] += delta[nargin++];
        }
      }
    }
    if (params.tN != 0) {
      if (!params.fixedTangential &&
          (!params.fixMainTangential || !params.fixOddTangential ||
           !params.fixEvenTangential)) {
        if (!params.fixMainTangential) {
          x_plus[5 + params.rN] += delta[nargin++];
          x_plus[5 + params.rN + 1] += delta[nargin++];
        }
        for (int i = 2; i < params.tN; ++i) {
          if (!params.fixEvenTangential && (i) % 2 == 0)
            x_plus[5 + params.rN + i] += delta[nargin++];
          if (!params.fixOddTangential && (i) % 2 == 1)
            x_plus[5 + params.rN + i] += delta[nargin++];
        }
      }
    }
    return true;
  }

  virtual bool ComputeJacobian(const double *, double *j) const {

    typedef Eigen::Matrix<double, -1, -1, Eigen::RowMajor> jt;
    Eigen::Map<jt> J(j, GlobalSize(), LocalSize());
    J.setZero();
    int nargin = 0;
    if (!params.fixedFocal) {
      J(0, nargin) = 1.0;
      if (params.equalFocal)
        J(1, nargin) = 1.0;
      else
        J(1, ++nargin) = 1.0;
      nargin++;
    }

    if (!params.fixedPrincipal) {
      J(2, nargin) = 1.0;
      if (params.equalPrincipal)
        J(3, nargin) = 1.0;
      else
        J(3, ++nargin) = 1.0;
      nargin++;
    }

    if (!params.fixedSkew && !params.zeroSkew) {
      J(4, nargin) = 1.0;
      nargin++;
    }

    if (params.rN != 0) {
      if (!params.fixedRadial &&
          (!params.fixOddRadial || !params.fixEvenRadial)) {
        for (int i = 0; i < params.rN; ++i) {
          if (!params.fixEvenRadial && (1 + i) % 2 == 0)
            J(5 + i, nargin++) = 1.0;
          if (!params.fixOddRadial && (1 + i) % 2 == 1)
            J(5 + i, nargin++) = 1.0;
        }
      }
    }

    if (params.tN != 0) {

      if (!params.fixedTangential &&
          (!params.fixMainTangential || !params.fixOddTangential ||
           !params.fixEvenTangential)) {
        if (!params.fixMainTangential) {
          J(5 + params.rN, nargin++) = 1.0;
          J(5 + params.rN + 1, nargin++) = 1.0;
        }
        for (int i = 2; i < params.tN; ++i) {
          if (!params.fixEvenTangential && (i) % 2 == 0)
            J(5 + params.rN + i, nargin++) = 1.0;
          if (!params.fixOddTangential && (i) % 2 == 1)
            J(5 + params.rN + i, nargin++) = 1.0;
        }
      }
    }
    return true;
  }
};

#endif // PINHOLELOCALPARAMETRIZATION_H

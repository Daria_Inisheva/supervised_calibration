#ifndef PINHOLECAMERA_H
#define PINHOLECAMERA_H

#include "camera_models/CameraModel.h"
#include "camera_models/PinHoleLocalParametrization.h"
#include <Eigen/Eigen>
#include <ceres/ceres.h>
#include <iostream>

typedef Eigen::Vector2d V2;
typedef Eigen::Vector3d V3;
typedef Eigen::VectorXd Vx;

// Here modelParams is
// 2 for focal
// 2 for principal
// 1 for skew
// radialN for radial distortion
// tangentialN for tangential distortion
class PinHoleModel : public CameraModel {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  explicit PinHoleModel(int w, int h, PinHoleCalibrationParams calibParams);

  PinHoleModel(const V2 &focal, const V2 &principal, const Vx &radial,
               const Vx &tangential, double skew = 0);

  int radialN;
  int tangentialN;
  PinHoleCalibrationParams calibParams;
  V2 getFocal() const;
  Eigen::Vector2d getPrincipal() override;
  double getSkew() const;
  Vx getRadialPolynom() const;
  Vx getTangentialPolynom() const;
  Eigen::Matrix3d getCalibMatrix() const;
  std::string getType() override { return "Pinhole"; }
  void setFocal(const V2 &focal);
  void setPrincipal(const V2 &principal);
  void setSkew(const double &skew);
  void print() override;
  void exportToMatlab(const std::string &fname) override;
  void exportToOpencv(const std::string &fname) override;
  void setRadialN(const int &N) { radialN = N; }
  void setTangentialN(const int &N) { tangentialN = N; }

  void saveToFile(const std::string &fname) override;
  void loadFromFile(const std::string &fname) override;
  template <typename T>
  static Eigen::Matrix<T, -1, 1>
  getModelParamsVector(const Eigen::Matrix<T, 2, 1> &focal,
                       const Eigen::Matrix<T, 2, 1> &principal, const T &skew,
                       const Eigen::Matrix<T, -1, 1> &radial,
                       const Eigen::Matrix<T, -1, 1> tangential) {
    Eigen::Matrix<T, -1, 1> params(2 + 2 + 1 + radial.rows() +
                                   tangential.rows());
    params << focal, principal, skew, radial, tangential;
    return params;
  }

  // Project 3D point to the image plane
  // camera_matrix is supposed to be Intr Matrix .data()
  template <typename T>
  static Eigen::Matrix<T, 2, 1>
  project(const Eigen::Matrix<T, 3, 1> &point,
          const Eigen::Matrix<T, 2, 1> &focal,
          const Eigen::Matrix<T, 2, 1> &principal, const T &skew,
          const Eigen::Matrix<T, -1, 1> &radial,
          const Eigen::Matrix<T, -1, 1> &tangential) {

    Eigen::Matrix<T, 3, 1> pt(point);

    if (pt(2) <= T(0.0))
      return principal;
    pt = pt / pt(2);

    Eigen::Matrix<T, 2, 1> distorted = distort(pt, radial, tangential);

    T x = distorted(0) * focal(0) + skew * distorted(1) + principal(0);
    T y = distorted(1) * focal(1) + principal(1);
    return Eigen::Matrix<T, 2, 1>(x, y);
  }

  template <typename T>
  Eigen::Matrix<T, 2, 1>
  project(const Eigen::Matrix<T, 3, 1> &pt,
          const Eigen::Matrix<T, -1, 1> &modelParams) const {
    return project(pt, modelParams, radialN, tangentialN);
  }
  template <typename T>
  static Eigen::Matrix<T, 2, 1>
  project(const Eigen::Matrix<T, 3, 1> &pt,
          const Eigen::Matrix<T, -1, 1> &modelParams, const int &nR,
          const int &nT) {
    Eigen::Matrix<T, 2, 1> focal = modelParams.template head<2>().eval(),
                           principal(modelParams[2], modelParams[3]);
    T skew = modelParams[4];
    Eigen::Matrix<T, -1, 1> radial(nR), tangential(nT);
    for (int i = 0; i < nR; ++i)
      radial[i] = modelParams[5 + i];

    for (int i = 0; i < nT; ++i)
      tangential[i] = modelParams[5 + nR + i];
    return project(pt, focal, principal, skew, radial, tangential);
  }

  V2 project(const V3 &point) override {
    return project(point, modelParams, radialN, tangentialN);
  }

  template <typename T>
  static Eigen::Matrix<T, 2, 1>
  distort(const Eigen::Matrix<T, 3, 1> &point,
          const Eigen::Matrix<T, -1, 1> &radial,
          const Eigen::Matrix<T, -1, 1> tangential) {

    T r = point.template head<2>().norm();
    if (r < Eigen::NumTraits<T>::epsilon())
      return point.template head<2>();

    T r_radial_step = r;
    T r_tangential_step = point.template head<2>().squaredNorm();
    // radial distortion
    //(1 + k1*r + k2*r^2 + ... )
    T common_radial = T(1.0);
    for (int i = 0; i < radial.rows(); ++i) {
      common_radial += radial[i] * r_radial_step;
      r_radial_step *= r;
    }

    T x_tg = T(0.0), //
        y_tg = T(0.0);
    // tangential
    // (2a1xy + a2(r^2 + 2x^2))(1 + a3r^2 + a4r^3 ..)
    // (a1(r^2 + 2y^2) + 2a2xy)(1 + a3r^2 + a4r^3 ..)
    // first part of tangential distortion

    T a1 = tangential.rows() >= 1 ? tangential[0] : T(0.0);
    T a2 = tangential.rows() >= 2 ? tangential[1] : T(0.0);

    x_tg = T(2.0) * a1 * point(0) * point(1) +
           a2 * (r * r + T(2.0) * point(0) * point(0));

    y_tg = T(2.0) * a2 * point(0) * point(1) +
           a1 * (r * r + T(2.0) * point(1) * point(1));

    T common_tg = T(1.0);
    for (int i = 2; i < tangential.rows(); ++i) {
      common_tg = common_tg + tangential[i] * r_tangential_step;
      r_tangential_step = r_tangential_step * r;
    }
    x_tg *= common_tg;
    y_tg *= common_tg;

    return Eigen::Matrix<T, 2, 1>(point(0) * common_radial + x_tg,
                                  point(1) * common_radial + y_tg);
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1> projectBack(
      const Eigen::Matrix<T, 2, 1> &point, const Eigen::Matrix<T, 2, 1> &focal,
      const Eigen::Matrix<T, 2, 1> &principal, const T &skew,
      const Eigen::Matrix<T, -1, 1> &radial,
      const Eigen::Matrix<T, -1, 1> &tangential, int maxIterations = 200) {

    typedef Eigen::Matrix<T, 3, 1> VT3;
    T y = (point(1) - principal(1)) / focal(1);
    T x = (point(0) - principal(0) - skew * y) / focal(0);
    VT3 pt(x, y, T(1.0));

    VT3 initial = undistort_radial(pt, radial);

    typedef ceres::Jet<T, 2> J2;

    for (int i = 0; i < maxIterations; ++i) {
      J2 x_dif(initial(0), 0), y_dif(initial(1), 1);

      Eigen::Matrix<J2, 3, 1> ray(x_dif, y_dif, J2(1.0));
      auto new_pixel = project(ray, focal.template cast<J2>().eval(),
                               principal.template cast<J2>().eval(), J2(skew),
                               radial.template cast<J2>().eval(),
                               tangential.template cast<J2>().eval());

      Eigen::Matrix<T, 2, 2> J;
      J << new_pixel(0).v[0], new_pixel(0).v[1], new_pixel(1).v[0],
          new_pixel(1).v[1];

      Eigen::Matrix<T, 2, 1> F_value(new_pixel(0).a, new_pixel(1).a);

      Eigen::Matrix<T, 2, 1> dx = J.inverse() * (point - F_value);

      if ((F_value - point).norm() < T(1e-16))
        break;

      initial.template head<2>() = initial.template head<2>() + dx;
    }

    return initial;
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1>
  projectBack(const Eigen::Matrix<T, 2, 1> &pt,
              const Eigen::Matrix<T, -1, 1> &modelParams, const int &nR,
              const int &nT) {
    Eigen::Matrix<T, 2, 1> focal = modelParams.template head<2>().eval(),
                           principal(modelParams[2], modelParams[3]);
    T skew = modelParams[4];
    Eigen::Matrix<T, -1, 1> radial(nR), tangential(nT);
    for (int i = 0; i < nR; ++i)
      radial[i] = modelParams[5 + i];

    for (int i = 0; i < nT; ++i)
      tangential[i] = modelParams[5 + nR + i];
    return projectBack(pt, focal, principal, skew, radial, tangential);
  }

  V3 projectBack(const V2 &point) override {
    return projectBack(point, modelParams, radialN, tangentialN);
  }

  template <typename T>
  static Eigen::Matrix<T, 3, 1>
  undistort_radial(const Eigen::Matrix<T, 3, 1> &point,
                   const Eigen::Matrix<T, -1, 1> &radial) {

    T r_pt = point.template head<2>().norm();

    // Getting initial value for reverse distortion
    Eigen::Matrix<T, Eigen::Dynamic, 1> polyCoeffs(radial.rows() + 2);
    polyCoeffs[0] = -r_pt;
    polyCoeffs[1] = T(1);
    for (int i = 0; i < radial.rows(); ++i)
      polyCoeffs[i + 2] = radial[i];

    T firstPosRoot = solve_equation(polyCoeffs);

    Eigen::Matrix<T, 2, 1> distorted;
    distorted = point.head(2).normalized() * firstPosRoot;
    return Eigen::Matrix<T, 3, 1>(distorted(0), distorted(1), T(1));
  }

  ceres::CostFunction *
  getGlobalRelativeCost(const Eigen::Vector2d &pixel,
                        const Eigen::Vector3d &pattern) override;

  ceres::LocalParameterization *
  getLocalParametrization(CameraCalibrationParams *params) override;

  ceres::LocalParameterization *
  getLocalParametrization(PinHoleCalibrationParams *params);

  void setCalibParams(CameraCalibrationParams *params) override;
  ceres::LocalParameterization *createLocalParametrization() override;

  ceres::CostFunction *
  getCameraConversionCost(const Eigen::Vector2d &pixel,
                          const Eigen::Vector3d &pattern) override;

  double convertFromModel(const std::shared_ptr<CameraModel> &from) override;

private:
  double convertOptimization(const std::shared_ptr<CameraModel> &from,
                             ceres::LossFunction *loss = nullptr);
};

#endif //__PINHOLECAMERA_H

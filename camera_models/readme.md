# Camera Models

__Supported camera models:__  
1. Pinhole with radial and tangential distortion model   
2. Atan-fisheye camera model  
3. Omnidirectional camera model  

# Pinhole model
Pinhole model implemented to be working with any distortion polynom degree.
This model is implemented in PinHoleModel class.
This model is described by Calibration matrix and 2 polynoms for radial and tangential distortion.
**Camera Calibration matrix**:
calibration matrix K looks like 
fx s px
0 fy py
0 0 1
where (fx, fy) = focal length, (px, py) - principal point, s - skew
__Project:__
projection of the point is K * Xdistort, where Xdistort = distorted point and K - calibration matrix.
### Distortion model
**Radial distortion polynom** :
radial distortion polynom coeffs lia in Eigen::VectorXd = {k1, k2 ,...} which is interpeted as  
f( r) = 1 + k1r + k2r^2 + ...
**Tangential distortion polynom** :
tangential distortion polynom coeffs lies in Eigen::VectorXd = {k1, k2 , k3, ...}  
fx(x, y, r) = (2k1xy + k2(r^2 + 2x^2))(1 + k3r^2 + ...)
fy(x, y, r) = (k1(r^2 + 2y^2) + 2k2xy)(1 + k3r^2 + ...)

Point ditortion is done by 
Xdistort = X * f( r) + fx(X, Y, r)
Ydistort = Y * f( r) + fy(X, Y, r)
where (X, Y) - point in normalized image coordinates (Xdistort, Ydistort) - distorted point

__Usage example:__

```
Eigen::Vector2d focal(1200, 1400);
Eigen::Vector2d principalPoint(500, 500);
double skew = 0.0;

Eigen::VectorXd radialDistortionPolynom(5);
radialDistortionPolynom<<1, 2, 3, 4, 5;

Eigen::VectorXd tangentialDistortionPolynom(2);
tangentialDistortionPolynom<<1, 2;
PinHoleModel pinhole(focal, principalPoint, radialDistortionPolynom, 
                     tangentialDistortionPolynom, skew);
pinhole.width = 120000;
// Project 3d point to the image plane
pinhole.project(Eigen::Vector3d(4, 5, 10));

// Project point or ray
pinhole.projectBack(Eigen::Vector2d(120,  250));
```
PinHoleModel also could be constructed by width, height and PinHoleCalibrationParams calss instance.

```
// Radial distortion has 8 elements in Vector, tangential - 2
PinHoleCalibrationParams params(8, 2);
params.zeroSkew = true; // skew param would be zero after calibration
PinHoleModel pinhole(my_width, my_height, params);
```
PinHoleCalibrationParams is used to store calibration restrictions: equalFocal or fixedPrincipal. 

# Omnidirectinal model
Omnidirectional model is same as in [matlab](https://www.mathworks.com/help/vision/ug/fisheye-calibration-basics.html), but size of polynom is not fixed to be 4.
This model consist of stretch Matrix, principal point and polynom.


__Usage example:__

```
// a b
// c 1
// StretchMatrix as Vector3d is (a, b, c);
Eigen::Vector3d stretchMatrix(1, 0, 0);// this could also be Matrix2d
Eigen::Vector2d principal(500, 500);
Eigen::VectorXd coeffs(3);
coeffs << 500.549, -0.000233596, -0.0000000125904;

OmnidirectionalModel omnidir(stretchMatrix, principal, coeffs);
omnidir.height = 120000;
// Project 3d point to the image plane
omnidir.project(Eigen::Vector3d(4, 5, 10));

// Project point or ray
omnidir.projectBack(Eigen::Vector2d(120,  250));
```
OmnidirectionalModel also could be constructed by width, height and OmnidirectionalCalibrationParams calss instance.

```
// Polynom has a degree of 5
OmnidirectionalCalibrationParams params(5);
params.identStretch = true; // Stretch Matrix would be ident after calibration
OmnidirectionalModel pinhole(my_width, my_height, params);
```
OmnidirectionalCalibrationParams is used to store calibration restrictions: fixedPrincipal or fixedPolynom. 

# Atan-fisheye model
Atan-fisheye model is based on [opencv fisheye model](https://docs.opencv.org/trunk/db/d58/group__calib3d__fisheye.html), but atan2 is used in stead of atan, so this model could project point, which are behind the camera.



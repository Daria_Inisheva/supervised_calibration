#include <camera_models/OmnidirectionalCamera.h>
#include <gtest/gtest.h>
#include <iostream>
#include <random>

double getOmnidirectionalNorm(Eigen::Vector3d p) {
  //  Eigen::Matrix2d stretchMatrix = Eigen::Matrix2d::Identity();
  Eigen::Vector2d stretchMatrix(1, 0);
  Eigen::Vector2d principal(500, 500);
  Eigen::VectorXd coeffs(3);
  coeffs(0) = 500.549;
  coeffs(1) = -0.000233596;
  coeffs(2) = -0.0000000125904;
  Eigen::Vector2d pixel =
      OmnidirectionalModel::project(p, stretchMatrix, principal, coeffs);
  Eigen::Vector3d out = OmnidirectionalModel::projectBack(pixel, stretchMatrix,
                                                          principal, coeffs);
  return (out.normalized() - p.normalized()).squaredNorm();
}

const int NTEST = 1000;

TEST(OmnidirectionalModelTest, FrontPointReproject) {
  std::uniform_real_distribution<double> runif(-20, 20);
  std::mt19937 rng;
  for (int i = 0; i < NTEST; ++i) {
    Eigen::Vector3d pt(runif(rng), runif(rng), runif(rng));
    if (pt.squaredNorm() == 0.0 || pt(2) == 0.0) {
      --i;
      continue;
    }
    EXPECT_GT(Eigen::NumTraits<double>::epsilon(), getOmnidirectionalNorm(pt));
  }
}

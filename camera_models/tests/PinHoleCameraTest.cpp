#include <camera_models/PinHoleCamera.h>
#include <gtest/gtest.h>
#include <iostream>
#include <random>

double getPinHoleNorm(const Eigen::Vector3d &p) {
  Eigen::Vector2d focal(1000, 1000);
  double skew = 0.1;
  Eigen::Vector2d principal(500, 500);
  Eigen::VectorXd radial(3);
  radial(0) = 0.01;
  radial(1) = 0.001;
  radial(2) = 0.0001;
  Eigen::VectorXd tangential(3);
  tangential(0) = 0.02;
  tangential(1) = 0.0045;
  tangential(2) = -0.00004;

  Eigen::Vector2d pixel =
      PinHoleModel::project(p, focal, principal, skew, radial, tangential);
  Eigen::Vector3d out = PinHoleModel::projectBack(pixel, focal, principal, skew,
                                                  radial, tangential);
  return (out.normalized() - p.normalized()).squaredNorm();
}

const int NTEST = 1000;

TEST(PinHoleModelTest, FrontPointReproject) {
  std::uniform_real_distribution<double> runif(-3, 3);
  std::mt19937 rng;
  for (int i = 0; i < NTEST; ++i) {
    Eigen::Vector3d pt(runif(rng), runif(rng), 1);
    if (pt.squaredNorm() == 0.0 || pt(2) == 0.0) {
      --i;
      continue;
    }
    EXPECT_GT(Eigen::NumTraits<double>::epsilon(), getPinHoleNorm(pt));
  }
}
